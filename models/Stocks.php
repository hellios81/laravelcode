<?php
/**
 * Stocks File Doc Comment
 * php version 7.*+
 * 
 * @category  Model
 * @package   Model_For_Stocks
 * @author    Srdjan Milicevic <webmaster@solveit.in.rs>
 * @copyright 2017 SolveIT d.o.o. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      https://solveit.in.rs
 */
namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Stocks Class Doc Comment
 * 
 * Class used to interact with Stocks table
 * 
 * @category  Class
 * @package   ModelForStocks
 * @author    Srdjan Milicevic <webmaster@solveit.in.rs>
 * @copyright 2017 SolveIT d.o.o. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      https://solveit.in.rs
 * 
 * @since 1.0.1
 */
class Stocks extends Model
{
    protected $fillable = [
        'storages_id', 
        'productionorders_id', 
        'value', 
        'invoices_id', 
        'goods_id', 
        'quantity', 
        'procurements_id', 
        'dispatches_id', 
        'requisitions_id', 
        'users_id'
    ];

    /**
     * Get Relation To Goods Model
     *
     * @return Model Relation
     */
    public function goods()
    {
        return $this->belongsTo('App\Goods');
    }

    /**
     * Get Relation To Procurements Model
     *
     * @return Model Relation
     */
    public function procurements()
    {
        return $this->belongsTo('App\Procurements');
    }

    /**
     * Get Relation To Invoices Model
     *
     * @return Model Relation
     */
    public function invoices()
    {
        return $this->belongsTo('App\Invoices');
    }

    /**
     * Get Relation To Requisitions Model
     *
     * @return Model Relation
     */
    public function requisitions()
    {
        return $this->belongsTo('App\Requisitions');
    }

    /**
     * Get Relation To Production Orders Model
     *
     * @return Model Relation
     */
    public function productionorders()
    {
        return $this->belongsTo('App\ProductionOrders');
    }

    /**
     * Create Accessor for Quantity
     * 
     * @param float $value Quantity of Stock
     *
     * @return Model Accessor
     */
    public function setQuantityAttribute($value)
    {
        $this->attributes['quantity'] = str_replace(',', '.', $value);
    }

    /**
     * Create Accessor for Price
     * 
     * @param float $value Price of Stock
     *
     * @return Model Accessor
     */
    public function setPriceAttribute($value)
    {
        $this->attributes['price'] = str_replace(',', '.', $value);
    }
}

