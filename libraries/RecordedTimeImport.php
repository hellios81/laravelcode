<?php
/**
 * RecordedTimeImport File Doc Comment
 * php version 7.*+
 * 
 * @category  Importer
 * @package   Import_For_Recorded_Time_From_Excel
 * @author    Srdjan Milicevic <webmaster@solveit.in.rs>
 * @copyright 2017 SolveIT d.o.o. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      https://solveit.in.rs
 */
namespace App\Imports;

use App\RecordedTime;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

/**
 * RecordedTimeImport Class Doc Comment
 * 
 * Class used to import rows into database from Excel file
 * 
 * @category  Class
 * @package   RecordedTimeImport
 * @author    Srdjan Milicevic <webmaster@solveit.in.rs>
 * @copyright 2018 SolveIT d.o.o. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      https://solveit.in.rs
 * 
 * @since 1.0.1
 */
class RecordedTimeImport implements ToModel, WithHeadingRow
{
    /**
     * Model Functions Used to Import Rows from Excel File
     * 
     * @param array $row Single Row from Excel Sheet
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        $date 
            = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject(
                $row['time']
            );
        if ($row['inout_state'] == 'Check-In') {
            $type = 'entry';
        } elseif ($row['inout_state'] == 'Check-Out') {
            $type = 'exit';
        }

        return new RecordedTime(
            [
                'employee_id' => str_slug($row['first_name'] . $row['last_name']) , 
                'date' => $date->format('Y-m-d') , 
                'time' => $date->format('H:i') , 
                'type' => $type, 
                'location' => $row['device_name']
            ]
        );
    }
}

