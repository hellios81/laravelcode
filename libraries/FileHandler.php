<?php
/**
 * File Handler File Doc Comment
 * php version 7.*+
 * 
 * @category  Helper_Library
 * @package   File_Handler_Used_For_AWS_S3_Manipulation
 * @author    Srdjan Milicevic <webmaster@solveit.in.rs>
 * @copyright 2017 SolveIT d.o.o. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      https://solveit.in.rs
 */
namespace App\FileServices;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

/**
 * File Handler Class Doc Comment
 * 
 * Class used to interact With Files in the App
 * 
 * @category  Class
 * @package   FileHandler
 * @author    Srdjan Milicevic <webmaster@solveit.in.rs>
 * @copyright 2017 SolveIT d.o.o. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      https://solveit.in.rs
 * 
 * @since 1.0.1
 */
class FileHandler
{
    /**
     * Calculate directory size -> sum of all file sizes in it
     *
     * @return integer $directory_size
     */
    public static function directorySize()
    {
        $directory_size = null;
        $directory_root_files 
            = \Storage::disk('s3')
            ->allFiles(\Auth::user()->id);
        foreach ($directory_root_files as $root_file) {
            $directory_size += \Storage::disk('s3')->size($root_file) / 1024 / 1024;
        }

        return $directory_size;
    }

    /**
     * Return URI's of all given files in the directory
     *
     * @return array $inner_files
     */
    public static function directoryFilesUri()
    {
        $s3 = \Storage::disk('s3');
        $client = $s3->getDriver()
            ->getAdapter()
            ->getClient();
        $bucket = \Config::get('filesystems.disks.s3.bucket');
        $files_in = $client->listObjects(
            [
                'Bucket' => $bucket, 
                'Prefix' => \Auth::user()->id . '/'
            ]
        );
        foreach ($files_in['Contents'] as $file_single) {
            $file_getter = $client->getCommand(
                'GetObject', 
                [
                    'Bucket' => $bucket, 
                    'Key' => $file_single['Key']
                ]
            );
            $request = $client->createPresignedRequest($file_getter, '+60 minutes');
            $inner_files[] = $request->getUri();
        };

        return $inner_files;
    }

    /**
     * Upload Files 
     * 
     * @param object $file     Actual File that is being uploaded
     * @param string $filename Name of the File that is being uploaded
     * @param string $share    Instruct if the file should be shared
     *
     * @return boolean
     */
    public static function uploadFile($file, $filename = null, $share = '')
    {
        if (FileHandler::directoryExists()) {
            
        } else {
            FileHandler::createDirectory();
        }
        $size_org = FileHandler::directorySize();
        if ($size_org > 150) {
            return redirect()->back()
                ->withFlashMessage('Out of space!!!')
                ->withFlashType('success');
        }
        if ($share == 'shared') {
            \Storage::disk('s3')->put($filename, $file);
        } elseif ($share == 'wall') {
            \Storage::disk('s3')->put(\Auth::user()->id . '/' . $filename, $file);
        } elseif ($share == 'social_media_data') {
            \Storage::disk('s3')->put(\Auth::user()->id . '/' . $filename, $file);
        } elseif (File::get($file)) {
            if ($filename) {
                \Storage::disk('s3')
                    ->put(
                        \Auth::user()->id . '/' . $filename, 
                        File::get($file)
                    );
            } else {
                \Storage::disk('s3')
                    ->put(
                        \Auth::user()->id . '/' . $file->getClientOriginalName(), 
                        File::get($file)
                    );
            }
        } else {
            \Storage::disk('s3')->put(\Auth::user()->id . '/' . $filename, $file);
        }
        return true;
    }

    /**
     * Check if directory exists
     * 
     * @param string $directory Directory
     *
     * @return boolean
     */
    public static function directoryExists($directory = null)
    {
        if ($directory) {
            $bool = \Storage::disk('s3')->exists(
                \Auth::user()->id . '/' . $directory
            );
        } else {
            $bool = \Storage::disk('s3')->exists(\Auth::user()->id . '/');
        }
        return $bool;
    }

    /**
     * Delete Files
     * 
     * @param string $file URI of a File
     *
     * @return integer $directory_size
     */
    public static function deleteFiles($file)
    {
        $explode = explode("/", $file, 2);
        if ($explode[0] == \Auth::user()->id) {
            \Storage::disk('s3')
                ->delete(\Auth::user()->id . '/' . $explode[1]);
        } else {
            \Storage::disk('s3')->delete(\Auth::user()->id . '/' . $file);
        }
    }

    /**
     * Create Directory for the Files
     * 
     * @param string $directory Name of the Directory
     *
     * @return void
     */
    public static function createDirectory($directory = null)
    {
        if ($directory) {
            \Storage::disk('s3')->makeDirectory(
                \Auth::user()->id 
                . '/' 
                . $directory
            );
        } else {
            \Storage::disk('s3')
                ->makeDirectory(\Auth::user()->id);
        }
    }

    /**
     * Create URI for the Files
     * 
     * @param string $filename Name of the File
     * @param string $prefix   Prefix of the File based on User ID
     *
     * @return string $uri URI of a given file
     */
    public static function fileUri($filename, $prefix = null)
    {
        $s3 = \Storage::disk('s3');
        if ($prefix == null) {
            $prefix = \Auth::user()->id;
        }
        $client = $s3->getDriver()
            ->getAdapter()
            ->getClient();
        $bucket = \Config::get('filesystems.disks.s3.bucket');
        $file_getter = $client->getCommand(
            'GetObject', 
            [
                'Bucket' => $bucket, 
                'Key' => $prefix . '/' . $filename
            ]
        );
        $request = $client->createPresignedRequest($file_getter, '+60 minutes');
        $inner_file = $request->getUri();

        return $inner_file;
    }

    /**
     * Download File
     * 
     * @param string $filename Name of the File
     * @param string $mimetype MIME of the File
     *
     * @return \Illuminate\Http\Response
     */
    public static function downloadFile($filename, $mimetype)
    {
        $file = \Storage::disk('s3')
            ->get(\Auth::user()->id . '/' . $filename);
        return response($file, 200)
            ->header('Content-Type', $mimetype)
            ->header(
                'Content-Disposition', 
                'attachment; filename="' . $filename . '"'
            );
    }

    /**
     * Get Download URI of a File
     * 
     * @param string $filename Name of the File
     *
     * @return string $file or NULL
     */
    public static function getFile($filename)
    {
        if (\Storage::disk('s3')->exists(\Auth::user()->id . '/' . $filename)) {
            $file = \Storage::disk('s3')->get(\Auth::user()->id . '/' . $filename);
            return $file;
        } elseif (\Storage::disk('s3')->exists($filename)) {
            $file = \Storage::disk('s3')->get($filename);
            return $file;
        } else {
            return null;
        }
    }
}

