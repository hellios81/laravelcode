<?php
/**
 * Convert File Doc Comment
 * php version 7.*+
 * 
 * @category  Library
 * @package   Convert
 * @author    Srdjan Milicevic <webmaster@solveit.in.rs>
 * @copyright 2015 SolveIT d.o.o. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      https://solveit.in.rs
 */

 /**
  * Convert Class Doc Comment
  * 
  * Converts a given integer (in range [0..1T-1], inclusive) into
  *   alphabetical format ("one", "two", etc.)
  * 
  * @category  Class
  * @package   ControllerRelatedToAccounts
  * @author    Srdjan Milicevic <webmaster@solveit.in.rs>
  * @copyright 2018 SolveIT d.o.o. All rights reserved.
  * @license   GNU General Public License version 2 or later; see LICENSE
  * @link      https://solveit.in.rs
  * 
  * @since 1.0.1
  */
class Convert
{

    /**
     * Show all available account orders for further inspection
     *
     * @param float  $number   Number intended for Conversion
     * @param string $currency Description of Currency in use
     * 
     * @return string
     */
    public static function convertTowords($number, $currency)
    {
        $hyphen = '';
        $separator = '';
        $negative = 'negative';
        if ($currency == 'RSD') {
            $decimal = 'dinarai';
            $conjunction = 'i';
        } elseif ($currency == 'EUR') {
            $decimal = 'euros/';
            $conjunction = 'and';
        }
        if ($currency == 'RSD') {
            $dictionary = array(
            0 => 'nula',
            1 => 'jedan',
            2 => 'dva',
            3 => 'tri',
            4 => 'četiri',
            5 => 'pet',
            6 => 'šest',
            7 => 'sedam',
            8 => 'osam',
            9 => 'devet',
            10 => 'deset',
            11 => 'jedanaest',
            12 => 'dvanaest',
            13 => 'trinaest',
            14 => 'četrnaest',
            15 => 'petnaest',
            16 => 'šesnaest',
            17 => 'sedamnaest',
            18 => 'osamnaest',
            19 => 'devetnaest',
            20 => 'dvadeset',
            30 => 'trideset',
            40 => 'četrdeset',
            50 => 'pedeset',
            60 => 'šezdeset',
            70 => 'sedamdeset',
            80 => 'osamdeset',
            90 => 'devedeset',
            100 => 'stotina',
            200 => 'dvestotine',
            2000 => 'dvehiljade',
            1000 => 'hiljada',
            1000000 => 'miliona',
            1000000000 => 'milijardi',
            1000000000000 => 'trillion',
            1000000000000000 => 'quadrillion',
            1000000000000000000 => 'quintillion'
            );
        }
        if ($currency == 'EUR') {
            $dictionary = array(
            0 => 'zero',
            1 => 'one',
            2 => 'two',
            3 => 'three',
            4 => 'four',
            5 => 'five',
            6 => 'six',
            7 => 'seven',
            8 => 'eight',
            9 => 'nine',
            10 => 'ten',
            11 => 'eleven',
            12 => 'twelve',
            13 => 'thirteen',
            14 => 'fourteen',
            15 => 'fifteen',
            16 => 'sixteen',
            17 => 'seventeen',
            18 => 'eighteen',
            19 => 'nineteen',
            20 => 'twenty',
            30 => 'thirty',
            40 => 'fourty',
            50 => 'fifty',
            60 => 'sixty',
            70 => 'seventy',
            80 => 'eighty',
            90 => 'ninety',
            100 => 'hundred',
            1000 => 'thousand',
            1000000 => 'million',
            1000000000 => 'milijardi',
            1000000000000 => 'trillion',
            1000000000000000 => 'quadrillion',
            1000000000000000000 => 'quintillion'
            );
        }
        if (!is_numeric($number)) {
            return false;
        }

        if (($number >= 0 
            && (int)$number < 0) 
            || (int)$number < 0 - PHP_INT_MAX
        ) {
            // overflow
            trigger_error(
                'convertTowords only accepts numbers between -' 
                . PHP_INT_MAX 
                . ' and ' 
                . PHP_INT_MAX, 
                E_USER_WARNING
            );
            return false;
        }

        if ($number < 0) {
            return $negative . Convert::convertTowords(abs($number), $currency);
        }

        $string = $fraction = null;

        if (strpos($number, '.') !== false) {
            list($number, $fraction) = explode('.', $number);
        }

        switch (true)
        {
        case $number < 21:
                $string = $dictionary[$number];
            break;
        case $number < 100:
                $tens = ((int)($number / 10)) * 10;
                $units = $number % 10;
                $string = $dictionary[$tens];
            if ($units) {
                    $string .= $hyphen . $dictionary[$units];
            }
            break;
        case $number < 1000:
                $hundreds = $number / 100;
                $remainder = $number % 100;
                $string = $dictionary[$hundreds] . $dictionary[100];
            if ($remainder) {
                $string .= $conjunction 
                    . Convert::convertTowords($remainder, $currency);
            }
            break;
        default:
                $baseUnit = pow(1000, floor(log($number, 1000)));
                $numBaseUnits = (int)($number / $baseUnit);
                $remainder = $number % $baseUnit;
                $string = Convert::convertTowords($numBaseUnits, $currency) 
                    . $dictionary[$baseUnit];
            if ($remainder) {
                $string .= $remainder < 100 ? $conjunction : $separator;
                $string .= Convert::convertTowords($remainder, $currency);
            }
            break;
        }

        if (null !== $fraction && is_numeric($fraction)) {
            $string .= $decimal;
            $string .= Convert::convertTowords((int)$fraction, $currency);
            if ($currency == 'RSD') {
                $string .= 'para';
            } elseif ($currency == 'EUR') {
                $string .= 'eurocents';
            }

        }

        return $string;
    }
}