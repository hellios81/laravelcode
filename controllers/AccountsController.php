<?php
/**
 * Accounts Controller File Doc Comment
 * php version 7.*+
 * 
 * @category  Controller
 * @package   Controller_For_Accounting
 * @author    Srdjan Milicevic <webmaster@solveit.in.rs>
 * @copyright 2017 Kadet d.o.o. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      https://solveit.in.rs
 */
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Comitents;
use App\Invoices;
use App\InvoiceItems;
use App\Orders;
use App\Goods;
use App\Stocks;
use App\Services;
use App\Accounts;
use App\Storages;
use App\AccountingItems;
use App\AccountOrders;
use Carbon\Carbon;

/**
 * HomeController Class Doc Comment
 * 
 * Class used to interact with Home table
 * 
 * @category  Class
 * @package   ControllerRelatedToAccounts
 * @author    Srdjan Milicevic <webmaster@solveit.in.rs>
 * @copyright 2018 SolveIT d.o.o. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      https://solveit.in.rs
 * 
 * @since 1.0.1
 */
class AccountsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show all available account orders for further inspection
     *
     * @param object $request              Laravel Request object
     * @param object $account_orders_model Instance used for query
     * 
     * @link   /kpr
     * @return Illuminate\Support\Facades\View with AccountOrdersModel
     */
    public function kpr(Request $request, AccountOrders $account_orders_model)
    {
        $account_orders = $account_orders_model->newQuery();
        if ($request->has('date_from') 
            && $request->input('date_from') != ''
        ) { 
            $account_orders
                ->where('date', '>', strtotime($request->input('date_from')));
        }
        if ($request->has('date_to') 
            && $request->input('date_to') != ''
        ) { 
            $account_orders->where(
                'date', 
                '<', 
                strtotime($request->input('date_to'))
            );
        }
        $account_orders = $account_orders->where('account_order', 'like', '5%')
            ->orderBy('account_order', 'asc')
            ->with('accountingitems')
            ->get();

        return view('accounting.kpr', compact('account_orders'));
    }

    /**
     * Show all existing account orders
     *
     * @return Illuminate\Support\Facades\View
     */
    public function kir()
    {
        return view('accounting.kir');
    }

    /**
     * Show all available commitents in the application
     *
     * @param object $request                Laravel Request object
     * @param object $accounting_items_model Instance used for query
     * 
     * @link   /comitentsbook
     * @return Illuminate\Support\Facades\View with ComitentsModel
     * and AccountingItemsModel
     */
    public function comitentsBook(
        Request $request, 
        AccountingItems $accounting_items_model
    ) {
        $comitents = Comitents::all();
        $comitent = null;
        $accounting_items = $accounting_items_model->newQuery();
        if ($request->has('comitents_id') 
            && $request->input('comitents_id') != 0
        ) {
            $accounting_items->where(
                'comitents_id', 
                $request->input('comitents_id')
            );
            $comitent = Comitents::find($request->input('comitents_id'));
        }
        if ($request->has('from_date') 
            && $request->input('from_date') != ''
        ) {
            $accounting_items->where('date', '>', $request->input('from_date'));
        }
        if ($request->has('to_date') 
            && $request->input('to_date') != ''
        ) {
            $accounting_items->where('date', '>', $request->input('to_date'));
        }
        if ($request->has('account_search') 
            || $request->has('comitents_id') 
            || $request->has('from_date') 
            || $request->has('to_date')
        ) {
            $accounting_items = $accounting_items->orderBy('id', 'asc')
                ->orderBy('accounts_id', 'asc')
                ->paginate(50)
                ->appends($request->except('page'));
            $account_orders = AccountOrders::whereIn(
                'id', 
                $accounting_items->pluck('account_orders_id')
            )   ->get()
                ->pluck('account_order', 'id');
        } else {
            $accounting_items = null;
            $account_orders = null;
        }

        return view('accounting.comitentsbook')
            ->with(
                compact(
                    'comitent', 
                    'accounting_items', 
                    'comitents', 
                    'account_orders'
                )
            );
    }

    /**
     * Show all Account Orders in the application
     *
     * @param object $request              Laravel Request object
     * @param object $account_orders_model Instance used for query
     * 
     * @link   /accountordersbook
     * @return Illuminate\Support\Facades\View with AccountOrdersModel
     */
    public function accountOrdersBook(
        Request $request, 
        AccountOrders $account_orders_model
    ) {
        $account_orders = $account_orders_model->newQuery();

        $account_orders = $account_orders->orderBy('account_order', 'asc')
            ->get();

        return view('accounting.accountordersbook')
            ->with(compact('account_orders'));
    }

    /**
     * Search for existing accounts
     *
     * @param object $request Laravel Request object
     * 
     * @link   /searchaccounts
     * @return object $account Instance of Accounts Model
     */
    public function searchAccounts(Request $request)
    {
        $account = $request->get('account');
        $accounts = Accounts::where('account', 'like', '%' . $account . '%')->get();

        return $accounts;
    }

    /**
     * Show all accounts in the book with their current state
     *
     * @param object $request                Laravel Request object
     * @param object $accounting_items_model Instance used for query
     * 
     * @link   /accountsbook
     * @return Illuminate\Support\Facades\View with ComitentsModel
     * and AccountingItemsModel
     */
    public function accountsBook(
        Request $request, 
        AccountingItems $accounting_items_model
    ) {
        $comitents = Comitents::all();
        $accounting_items = $accounting_items_model->newQuery();
        if ($request->has('account_search') 
            && $request->input('account_search') != 0
        ) {
            $accounts = Accounts::where(
                'account', 
                'like', 
                $request->input('account_search') . '%'
            )   ->get()
                ->pluck('id')
                ->toArray();
            $accounting_items->whereIn('accounts_id', $accounts);
            $accounts = Accounts::where(
                'account', 
                'like', 
                $request->input('account_search') . '%'
            )   ->get()
                ->pluck('account', 'id');
        } else {
            $accounts = Accounts::get()->pluck('account', 'id');
        }
        if ($request->has('comitents_id') 
            && $request->input('comitents_id') != 0
        ) {
            $accounting_items->where(
                'comitents_id', 
                $request->input('comitents_id')
            );
        }
        if ($request->has('date_from') 
            && $request->input('date_from') != ''
        ) {
            $accounting_items->whereDate(
                'date', 
                '>=', 
                Carbon::parse($request->input('date_from'))
            );
        }
        if ($request->has('date_to') 
            && $request->input('date_to') != ''
        ) {
            $accounting_items->whereDate(
                'date', 
                '<=', 
                Carbon::parse($request->input('date_to'))
            );
        }
        if ($request->has('account_search') 
            || $request->has('comitents_id') 
            || $request->has('date_from') 
            || $request->has('date_to')
        ) {
            $accounting_items = $accounting_items->orderBy('accounts_id', 'asc')
                ->get();
            $account_orders = AccountOrders::whereIn(
                'id', 
                $accounting_items->pluck('account_orders_id')
            )   ->get()
                ->pluck('account_order', 'id');
        } else {
            $accounting_items = null;
            $account_orders = null;
        }

        return view('accounting.accountsbook')->with(
            compact(
                'accounting_items', 
                'comitents', 
                'accounts', 
                'account_orders'
            )
        );
    }

    /**
     * Add items to Accounting Order
     *
     * @param array $accounting_info Details of Accounting Order Items 
     * 
     * @return void
     */
    public static function addAccounting($accounting_info = null)
    {
        //određivanje poslednjeg postojećeg naloga u grupi
        $account_order_type = $accounting_info['account_order_type'];
        $account_order_additional_id 
            = $accounting_info['account_order_additional_id'];
        $account_order_name 
            = $account_order_type 
            . str_pad(
                $account_order_type, 
                5, 
                $account_order_additional_id, 
                STR_PAD_RIGHT
            );
        $last_account_order = AccountOrders::where(
            'account_order', 
            $account_order_name
        )->first();
        if (!isset($last_account_order)) {
            $last_account_order 
                = str_pad($account_order_type, 5, '0', STR_PAD_RIGHT) 
                + 1;
        }
        //spremanje niza za pravljenje naloga
        $account_order_data['account_order'] = $last_account_order;
        $account_order_data['date'] = $accounting_info['date'];
        $account_order_data['user_id'] = \Auth::user()->id;
        $account_order_data['description'] = $accounting_info['description'];
        //pravljenje naloga
        $account_orders = AccountOrders::updateOrCreate(
            ['account_order' => $account_order_data['account_order']], 
            $account_order_data
        );
        //stavke u nalogu
        $accounting_items = $accounting_info['accounts'];
        foreach ($accounting_items as $key => $accounting_item) {
            $explode = explode(':', $key);
            $account_item_data['date'] = $accounting_info['date'];
            $account_item_data['description'] = $accounting_info['item_description'];
            if ($explode[1] == 'd') {
                $account_item_data['debit'] = $accounting_item;
                $account_item_data['credit'] = 0;
            }
            if ($explode[1] == 'c') {
                $account_item_data['debit'] = 0;
                $account_item_data['credit'] = $accounting_item;
            }
            $account = Accounts::where('account', $explode[0])->first();
            if (!$account) {
                $account = Accounts::create(
                    [
                        'account' => $explode[0], 
                        'account_name' => 'Nije određeno!', 
                        'analysis' => 0
                    ]
                );
            }
            $account_item_data['comitents_id'] = $accounting_info['comitents_id'];
            $account_item_data['accounts_id'] = $account->id;
            $account_item_data['account_order_id'] = $account_orders->id;
            $account_item_data['storages_id'] = $accounting_info['storages_id'];
            $account_item_data['users_id'] = \Auth::user()->id;

            AccountingItems::updateOrCreate(
                [
                    'accounts_id' => $account_item_data['accounts_id'], 
                    'comitents_id' => $account_item_data['comitents_id'], 
                    'account_orders_id' => $account_orders->id
                ], 
                $account_item_data
            );
        }
        //sredjivanje dugovanja i potrazivanja u nalogu
        $credit_sum = 0;
        $debit_sum = 0;
        $accounting_items = AccountingItems::where(
            'account_orders_id', 
            $account_orders->id
        )   ->get();
        foreach ($accounting_items as $item) {
            $credit_sum += $item->credit;
            $debit_sum += $item->debit;
        }
        $account_orders->credit = $credit_sum;
        $account_orders->debit = $debit_sum;
        $account_orders->save();

    }

    /**
     * Show all available commitents in the application
     *
     * @param integer $account_order_id Id used for Accounting Items
     * 
     * @link   /deleteaccountorder
     * @method POST
     * @return void
     */
    public function deleteAccountOrder($account_order_id = null)
    {
        $accounting_items = AccountingItems::where(
            'account_orders_id', 
            $account_order_id
        )->delete();
        AccountOrders::destroy($account_order_id);

        return redirect()->back();
    }

    /**
     * Show latest Account Order enterd into application
     *
     * @param object $request Illuminate\Http\Request
     * 
     * @link   /checkforlatestaccountorder
     * @return Illuminate\Http\Response
     */
    public function checkForLatestAccountOrder(Request $request)
    {
        $account_order = $request->input('account_order');
        $last_account_order_try = AccountOrders::where(
            'account_order', 
            'like', 
            $account_order . '%'
        )->orderBy('account_order', 'desc')
            ->first();
        if ($last_account_order_try) {
            $last_account_order = $last_account_order_try->account_order;
        }
        if (isset($last_account_order)) {
            $last_account_order = (int)$last_account_order + 1;
        } else {
            $last_account_order = str_pad($account_order, 5, '0', STR_PAD_RIGHT) + 1;
        }

        $response = [
            'success' => 200, 
            'value' => ['last_account_order' => $last_account_order]
        ];
        return response()->json($response);
    }

    /**
     * Completes latest Account Order entered into application
     *
     * @param object $request Illuminate\Http\Request
     * 
     * @link   /completeaccountorder
     * @method POST
     * @return Illuminate\Http\Response
     */
    public function completeaccountorder(Request $request)
    {
        $account_orders_id = $request->input('account_orders_id');
        $account_order = AccountOrders::find($account_orders_id);
        $account_order->completed = true;
        $account_order->save();

        return redirect()
            ->to('/accountorders');
    }

    /**
     * Delete Account Order Item from current Account Order
     *
     * @param integer $account_item_id Id of Account Item to be deleted
     * 
     * @link   /deleteaccountitem
     * @method POST
     * @return void
     */
    public function deleteAccountItem($account_item_id)
    {
        $account_item = AccountingItems::find($account_item_id);
        AccountingItems::destroy($account_item_id);
        $accounting_items = AccountingItems::where(
            'account_orders_id', 
            $account_item->account_orders_id
        )   ->get();
        $credit_sum = 0;
        $debit_sum = 0;
        foreach ($accounting_items as $item) {
            $credit_sum += $item->credit;
            $debit_sum += $item->debit;
        }
        $account_orders = AccountOrders::where(
            'id', 
            $account_item->account_orders_id
        )   ->first();
        $account_orders->credit = $credit_sum;
        $account_orders->debit = $debit_sum;
        $account_orders->save();

        return redirect()
            ->back();
    }

    /**
     * Add Accounting Item to current Account Order
     *
     * @param object $request Illuminate\Http\Request
     * 
     * @link   /addaccountitem
     * @method POST
     * @return Date
     */
    public function addAccountItem(Request $request)
    {
        $message = [
            'required' => 'Neophodan podatak!', 
            'unique' => 'Podatak već postoji u bazi!', 
            'min' => 'Mora imati najmanje :min znakova!', 
            'digits' => 'Mora imati :digits brojeva u oznaci!', 
            'required_without' 
            => 'Polje :attribute je obavezno, kada susedno polje nije upotrebljeno!'
        ];
        $validator = Validator::make(
            $request->all(), 
            [
                'accounts_id' => 'required', 
                'date' => 'required', 
                'debit' => 'required_without:credit', 
                'credit' => 'required_without:debit'
            ], 
            $message
        );
        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)->withInput();
        }
        $data = $request->all();
        if (is_null($request->input('description'))) {
            $data['description'] = '';
        }
        if (is_null($data['credit'])) {
            $data['credit'] = 0;
        }
        if (is_null($data['debit'])) {
            $data['debit'] = 0;
        }
        AccountingItems::create($data);
        $credit_sum = 0;
        $debit_sum = 0;
        $accounting_items = AccountingItems::where(
            'account_orders_id', 
            $request->input('account_orders_id')
        )   ->get();
        foreach ($accounting_items as $item) {
            $credit_sum += $item->credit;
            $debit_sum += $item->debit;
        }
        $account_orders = AccountOrders::where(
            'id', 
            $request->input('account_orders_id')
        )   ->first();
        $account_orders->credit = $credit_sum;
        $account_orders->debit = $debit_sum;
        $account_orders->save();

        return redirect()
            ->back()
            ->with(['date' => $data['date']]);
    }

    /**
     * Show all Accounting Items in a given Account Order
     *
     * @param integer $account_orders_id Id of searched Account Order
     * 
     * @link   /accountitems
     * @return Illuminate\Support\Facades\Views with 
     * AccountsModel, $show_only boolean, AccountOrdersModel,
     * ComitentsModel and AccountingItemsModel
     */
    public function accountItems($account_orders_id)
    {
        $account_order = AccountOrders::find($account_orders_id);
        $accounting_items = AccountingItems::where(
            'account_orders_id', 
            $account_orders_id
        )->get();
        $comitents = Comitents::all();
        $accounts = Accounts::all();
        $show_only = 0;

        return view('accounting.accountitems')
            ->with(
                compact(
                    'accounts', 
                    'show_only', 
                    'account_order', 
                    'accounting_items', 
                    'comitents'
                )
            );
    }

    /**
     * Show latest Account Order enterd into application
     *
     * @param object $request             Illuminate\Http\Request
     * @param object $account_order_model Instance of AccountOrders for query
     * 
     * @link   /storeaccountorders
     * @method POST
     * @return Illuminate\Http\Redirect
     */
    public function storeAccountOrders(
        Request $request, 
        AccountOrders $account_order_model
    ) {
        if ($request->input('button') == 'save') {
            $message = [
                'required' => 'Neophodan podatak!', 
                'unique' => 'Podatak već postoji u bazi!', 
                'min' => 'Mora imati najmanje :min znakova!', 
                'digits' => 'Mora imati :digits brojeva u oznaci!'
            ];
            $validator = Validator::make(
                $request->all(), 
                ['account_order' => 'required|digits:5', ], 
                $message
            );
            if ($validator->fails()) {
                return redirect('/accountorders')
                    ->withErrors($validator)->withInput();
            }
            $data = $request->all();

            AccountOrders::updateOrCreate(
                [
                    'account_order' => $request->input('account_order') 
                ], 
                $data
            );
        }
        if ($request->input('button') == 'search') {
            $account_orders = $account_order_model->newQuery();
            if ($request->has('account_order_search') 
                && $request->input('account_order_search') != 0
            ) { 
                $account_orders->where(
                    'account_order', 
                    'like', 
                    $request->input('account_order_search') . '%'
                );
            }
            if ($request->has('from_date') 
                && $request->input('from_date') != ''
            ) { 
                $account_orders->where('date', '>', $request->input('from_date'));
            }
            if ($request->has('to_date') 
                && $request->input('to_date') != ''
            ) { 
                $account_orders->where('date', '>', $request->input('to_date'));
            }

            $account_orders = $account_orders->orderBy('created_at', 'desc')
                ->paginate(25)
                ->appends($request->except('page'));

            return view('accounting.accountorders')
                ->with(compact('account_orders'));
        }

        return redirect()
            ->back();
    }

    /**
     * Show All Account Orders
     *
     * @param object $request             Illuminate\Http\Request
     * @param object $account_order_model AccountOrderModel for Query
     * 
     * @link   /accountorders
     * @return Illuminate\Support\Facades\View with AccountOrdersModel
     */
    public function accountOrders(
        Request $request, 
        AccountOrders $account_order_model
    ) {
        $account_order_query = $account_order_model->newQuery();
        if ($request->has('account_order') 
            && $request->input('account_order') != 0
        ) {
            $account_order_query->where(
                'account_order', 
                'like', 
                $request->input('search_category') . '%'
            );
        }
        $account_orders = $account_order_query->orderBy('created_at', 'desc')
            ->paginate(25)
            ->appends($request->except('page'));

        return view('accounting.accountorders')
            ->with(compact('account_orders'));
    }

    /**
     * Show All Accounts
     *
     * @link   /accounts
     * @method GET
     * @return Illuminate\Support\Facades\View
     */
    public function index()
    {
        $accounts = Accounts::orderBy('account', 'asc')->paginate(50);

        return view('accounting.accounts')
            ->with(compact('accounts'));
    }

    /**
     * Store Account
     *
     * @param object $request Illuminate\Http\Request
     * 
     * @link   /accounts/{params}
     * @method POST
     * @return Illuminate\Http\Redirect
     */
    public function store(Request $request)
    {
        if ($request->input('button') == 'save') {
            $message = [
                'required' => 'Neophodan podatak!', 
                'unique' => 'Podatak već postoji u bazi!', 
                'min' => 'Mora imati najmanje :min znakova!'
            ];
            $validator = Validator::make(
                $request->all(), 
                [
                    'account' => 'required', 
                    'account_name' => 'required'
                ], 
                $message
            );
            if ($validator->fails()) {
                return redirect('/accounts')
                    ->withErrors($validator)->withInput();
            }
            Accounts::updateOrCreate(
                [
                    'account' => $request->input('account') 
                ], 
                $request->all()
            );
        }
        if ($request->input('button') == 'search') {
            $accounts = Accounts::where('account', $request->input('account'))
                ->orWhere('account_name', $request->input('account_name'))
                ->paginate(25);

            return view('accounting.accounts')
                ->with(compact('accounts'));
        }

        return redirect()
            ->back();
    }

}
