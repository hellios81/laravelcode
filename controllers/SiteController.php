<?php
/**
 * Site Controller File Doc Comment
 * php version 7.*+
 * 
 * @category  Controller
 * @package   Controller_For_Edditing_Options_In_Site
 * @author    Srdjan Milicevic <webmaster@solveit.in.rs>
 * @copyright 2017 Kadet d.o.o. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      https://solveit.in.rs
 */
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use App\Sliders;
use App\GoodsCategories;

/**
 * SiteController Class Doc Comment
 * 
 * Class used to interact with Acquisitions table
 * 
 * @category  Class
 * @package   ControllerRelatedToEverythingOnSite
 * @author    Srdjan Milicevic <webmaster@solveit.in.rs>
 * @copyright 2017 Kadet d.o.o. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      https://solveit.in.rs
 * 
 * @since 1.0.1
 */
class SiteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the site sliders.
     *
     * @return \Illuminate\Http\View
     */
    public function sliderview()
    {
        $images = Sliders::whereNotNull('slider_id')
            ->orderBy('slide_order', 'asc')->get();
        return view('slider.sliderview')->with(compact('images'));
    }

    /**
     * Show view to create new site sliders.
     *
     * @return \Illuminate\Http\View
     */
    public function newslide()
    {
        return view('slider.newslide');
    }

    /**
     * Show the view to edit site sliders.
     *
     * @param integer $image_id id of image that should be edited
     * 
     * @return \Illuminate\Http\View
     */
    public function slideimageedit($image_id = null)
    {
        $image = Sliders::find($image_id);
        $categories = GoodsCategories::all();
        
        return view('slider.slideedit')->with(compact('image', 'categories'));
    }

    /**
     * Show the view to edit site sliders translation.
     *
     * @param object $request made of \Illuminate\Http\Request
     * 
     * @return \Illuminate\Http\View
     */
    public function saveslidetranslation(Request $request)
    {
        $image_id = $request->input('image_id');
        $title = $request->input('title');
        $body = $request->input('body');
        $category = $request->input('category');
        $image = Sliders::find($image_id);
        $image->title = $title;
        $image->body = $body;
        $image->url = $category;
        $image->save();

        return redirect()->back();
    }

    /**
     * Function to create site sliders.
     *
     * @param object $request made of \Illuminate\Http\Request
     * 
     * @return \Illuminate\Http\View
     */
    public function postnewslide(Request $request)
    {
        $input = $request->all();
        $imgData = json_decode($input["imgData"]);
        $x_offset = intval($imgData->x);
        $y_offset = intval($imgData->y);
        $width = intval($imgData->width);
        $height = intval($imgData->height);
        $image = $request->file('image');
        $database_image = new Sliders;
        if (Sliders::orderBy('id', 'desc')->first()) {
            $new_image_name = intval(Sliders::orderBy('id', 'desc')->first()->id)+1;
        } else {
            $new_image_name = 1;
        }
        $image_real_path = $image->getRealPath();
        $new_image_name .= '.'.$image->getClientOriginalExtension();
        $database_image->image_name = $new_image_name;
        Image::configure(array('driver' => 'imagick'));
        $file = Image::make($image_real_path);
        $file->crop($width, $height, $x_offset, $y_offset);
        $file->save(public_path('images/slider/'.$new_image_name));
        $database_image->slider_id = 1;
        $images = Sliders::whereNotNull('slider_id')
            ->orderBy('slide_order', 'asc')->get();
        if (!$images->isEmpty()) {
            $slide_order = intval($images->last()->slide_order)+1;
        } else {
            $slide_order = 1;
        }
        $database_image->slide_order = $slide_order;
        $database_image->save();

        return redirect()->to('/sliderview');
    }

    /**
     * Show the view to edit slide order.
     *
     * @param object $request made of \Illuminate\Http\Request
     * 
     * @return \Illuminate\Http\View
     */
    public function updateslideord(Request $request)
    {
        $image_id = $request->input('image_id');
        $requests = $request->all();
        $image = Sliders::find($image_id);
        if (array_key_exists('down', $requests)) {
            $down_order = intval($image->slide_order) + 1;
            $image_down = Sliders::where('slide_order', $down_order)->first();
            if ($image_down) {
                $image_down->slide_order = $image->slide_order;
                $image_down->save();
            }
            $image->slide_order = $down_order;
            $image->save();
        }
        if (array_key_exists('up', $requests)) {
            $up_order = intval($image->slide_order) - 1;
            $image_up = Sliders::where('slide_order', $up_order)->first();
            if ($image_up) {
                $image_up->slide_order = $image->slide_order;
                $image_up->save();
            }
            $image->slide_order = $up_order;
            $image->save();
        }

        return redirect()->back();
    }

      /**
       * Show images in site.
       *
       * @param object $image Images
       * 
       * @return Image
       */
    public function getimages($image)
    {
        $storagePath = storage_path($image);
        Image::configure(array('driver' => 'imagick'));
        
        return Image::make($storagePath)->response();
    }

    /**
     * Delete site sliders.
     *
     * @param object $request made of \Illuminate\Http\Request
     * 
     * @return \Illuminate\Http\View
     */
    public function deleteimage(Request $request)
    {
        $image_id = $request->input('image_id');
        $previous_url = $request->input('previous_url');
        $image_location = $request->input('image_location');
        $image = Sliders::find($image_id);
        if (file_exists(storage_path('/slider/'.$image->image_name))) {
            $test = unlink(storage_path($image_location.$image->image_name));
        }
        Sliders::destroy($image_id);
        
        return redirect()->to($previous_url);
    }

    /**
     * Show the site sliders.
     *
     * @param object $image Image
     * 
     * @return \Illuminate\Http\View
     */
    public function sliderimages($image)
    {
        $storagePath = storage_path('slider/'.$image);
        Image::configure(array('driver' => 'imagick'));
        
        return Image::make($storagePath)->response();
    }

    /**
     * Create new slider image through AJAX.
     *
     * @param object $request made of \Illuminate\Http\Request
     * 
     * @return \Illuminate\Http\Resposne
     */
    public function newimageajax(Request $request)
    {
        if ($request->isXmlHttpRequest() ) {
            $filename=null;
            $input = $request->all();
            $page_id = $request->pageId;
            $imgData = json_decode($input["imgData"]);
            $x_offset = intval($imgData->x);
            $y_offset = intval($imgData->y);
            $width = intval($imgData->width);
            $height = intval($imgData->height);
            $image = $request->file('image');
            if (array_key_exists('image_id', $input)) {
                $image_id = $input["image_id"];
            } else { 
                $image_id = 0;
            }
            if ($image_id == 0) {
                $database_image = new Sliders;
            }
            if (!is_null(Sliders::orderBy('id', 'desc')->first())) {
                $new_image_name = intval(
                    Sliders::orderBy('id', 'desc')->first()->id
                )+1;
            } else {
                $new_image_name = 1;
            }
        } else {
            $database_image = Sliders::find($image_id);
            $new_image_name = $database_image->id;
        }
        if ($image == null) {
            $image = $request->input('image');
            $explode = explode('/', $image);
            $image_real_name = end($explode);
            $explode_image_name = explode('.', $image_real_name);
            $old_image_name = $explode_image_name[0].'.'.$explode_image_name[1];
            $new_image_name .= '.'.$explode_image_name[1];
            $image_real_path = storage_path('/media/' . $old_image_name);
            $database_image->package_id = $input["package_id"];
            $database_image->package_image_purpose = $input["thumb-db-name"];
        } else {
            $image_real_path = $image->getRealPath();
            $new_image_name .= '.'.$image->getClientOriginalExtension();
            $original_name = $image->getClientOriginalName();
        }
        $database_image->image_name = $new_image_name;
        if ($page_id) {
            $database_image->page_id = $page_id;
        }
        if ($original_name) {
            $database_image->image_original_name = $original_name;
        }
        Image::configure(array('driver' => 'imagick'));
        $file = Image::make($image_real_path);
        $file->crop($width, $height, $x_offset, $y_offset);
        $file->save(storage_path('media/' . $new_image_name));
        $response = ['success'=>200, 'mediaUrl'=>'/admin/media/' . $new_image_name];
        $database_image->save();
        
        return response()->json($response);
    }
}