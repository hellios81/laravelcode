<?php
/**
 * Social Networks Controller File Doc Comment
 * php version 7.*+
 * 
 * @category  Controller
 * @package   Controller_For_Connecting_To_Social_Networks
 * @author    Srdjan Milicevic <webmaster@solveit.in.rs>
 * @copyright 2015 SolveIT d.o.o. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      https://solveit.in.rs
 */
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use App\Fb;
use DirkGroenen\Pinterest\Pinterest;
use App\SocialToken;
use App\Tumblr\TumblrOAuth;
use App\Reddit\RedditOAuth;
use Ixudra\Curl\Facades\Curl;
use GuzzleHttp\Client;
use Thujohn\Twitter\Facades\Twitter;
use Route;
use Illuminate\Http\Response;
use App\FileServices\FileHandler;

/**
 * SocialNetworksController Class Doc Comment
 * 
 * Class used to interact with Social Networks
 * 
 * @category  Class
 * @package   ControllerRelatedToSocialNetworks
 * @author    Srdjan Milicevic <webmaster@solveit.in.rs>
 * @copyright 2015 SolveIT d.o.o. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      https://solveit.in.rs
 * 
 * @since 1.0.1
 */
class SocialNetworksController extends Controller
{
    
    /**
     * Get Login URL for connecting to Facebook
     *
     * @return \Illuminate\Http\Redirect
     */
    public function facebookConnect()
    {
        $fb = \App::make('SammyK\LaravelFacebookSdk\LaravelFacebookSdk');
        $login_url = $fb->getLoginUrl(['email', 'user_posts']);

        return redirect($login_url);
    }

    /**
     * Get Login URL for connecting to Facebook
     * 
     * @param SDKInstance $fb Instance of SammyK\LaravelFacebookSDK
     *
     * @return \Illuminate\Http\Redirect
     */
    public function facebookCallback(
        \SammyK\LaravelFacebookSdk\LaravelFacebookSdk $fb
    ) {
        $database_token = SocialToken::find(\Auth::user()->id);
        if ($database_token->facebook_oauth_token) {
            $token = $database_token->facebook_oauth_token;
        } else {
            try
            {
                $token = $fb->getAccessTokenFromRedirect();
            }
            catch(\Facebook\Exceptions\FacebookSDKException $e)
            {
                dd($e->getMessage());
            }
            if (!$token) {
                $helper = $fb->getRedirectLoginHelper();
                if (!$helper->getError()) {
                    abort(403, 'Unauthorized action.');
                }
                dd(
                    $helper->getError(), 
                    $helper->getErrorCode(), 
                    $helper->getErrorReason(), 
                    $helper->getErrorDescription()
                );
            }
            if (!$token->isLongLived()) {
                $oauth_client = $fb->getOAuth2Client();
                try
                {
                    $token = $oauth_client->getLongLivedAccessToken($token);
                }
                catch(\Facebook\Exceptions\FacebookSDKException $e)
                {
                    dd($e->getMessage());
                }
            }
            $database_token->update(['facebook_oauth_token' => $token]);
        }
        $fb->setDefaultAccessToken($token);
        \Session::put('fb_user_access_token', (string)$token);
        try
        {
            $response = $fb->get('/me?fields=id,name,email');
        }
        catch(\Facebook\Exceptions\FacebookSDKException $e)
        {
            dd($e->getMessage());
        }
        $facebook_user = $response->getGraphUser();
        $user_action = '/' . $facebook_user['id'] . '/feed';
        try
        {
            $response2 = $fb->get($user_action);
        }
        catch(\Facebook\Exceptions\FacebookSDKException $e)
        {
            dd($e->getMessage());
        }
        $fb_array = array();
        foreach ($response2->getGraphEdge() as $one) {
            $array = (array)$one;
            $array = array_shift($array);
            $post_id = $array['id'];
            $post_user_action 
                = '/' 
                . $post_id 
                . '?fields=id,full_picture,picture,name,message,story,created_time';
            $response3 = $fb->get($post_user_action);
            $response4 = $response3->getGraphObject();
            $picture = null;
            $response5 = (array)$response4;
            $post_array = array_shift($response5);
            if (array_key_exists('picture', $post_array)) {
                $picture = $post_array['picture'];
            }
            if (array_key_exists('story', $post_array)) {
                $fb_array[] = [
                    'msg' => $post_array['story'], 
                    'timestamp' => $post_array['created_time'], 
                    'post_id' => $post_array['id'], 
                    'picture' => $picture
                ];

            }
            if (array_key_exists('message', $post_array)) {
                $fb_array[] = [
                    'msg' => $post_array['message'], 
                    'user_id' => \Auth::user()->id, 
                    'timestamp' => $post_array['created_time'], 
                    'post_id' => $post_array['id'], 
                    'picture' => $picture
                ];
            }
        }
        FileHandler::uploadFile(
            serialize($fb_array), 
            'facebook_' . \Auth::user()->id . '.snd', 
            'social_media_data'
        );

        return redirect('/live');
    }

    /**
     * Get Login URL for connecting to Twitter
     *
     * @return \Illuminate\Http\Redirect
     */
    public function twitterLogin()
    {
        $sign_in_twitter = true;
        $force_login = false;
        \Twitter::reconfig(['token' => '', 'secret' => '']);
        $token = \Twitter::getRequestToken(route('twitterredirect'));
        if (isset($token['oauth_token_secret'])) {
            $url = \Twitter::getAuthorizeURL($token, $sign_in_twitter, $force_login);
            \Session::put('oauth_state', 'start');
            \Session::put('oauth_request_token', $token['oauth_token']);
            \Session::put(
                'oauth_request_token_secret', 
                $token['oauth_token_secret']
            );
            return redirect($url);
        }
        return redirect('twitter.error');
    }

    /**
     * Get Redirect URL for connecting to Twitter
     *
     * @return \Illuminate\Http\Redirect
     */
    public function twitterRedirect()
    {
        if (\Session::has('oauth_request_token')) {
            $request_token = [
                'token' => \Session::get('oauth_request_token') , 
                'secret' => \Session::get('oauth_request_token_secret') 
            ];
            \Twitter::reconfig($request_token);
            $oauth_verifier = false;
            if (\Input::has('oauth_verifier')) {
                $oauth_verifier = \Input::get('oauth_verifier');
            }
            $token = \Twitter::getAccessToken($oauth_verifier);
            if (!isset($token['oauth_token_secret'])) {
                return redirect('twitter.login')
                    ->with('flash_error', 'We could not log you in on \Twitter.');
            }
            $credentials = \Twitter::getCredentials();

            if (is_object($credentials) && !isset($credentials->error)) {
                \Session::put('access_token', $token);
                $user_auth = User::find(\Auth::user()->id);
                $user_auth->update(['twiter' => $token['screen_name']]);
                if (\Session::has('oauth_request_token')) {
                    try
                    {
                        $twitter = Twitter::getHomeTimeline(
                            [
                                'screen_name' => \Auth::user()->twiter, 
                                'count' => 20, 
                                'format' => 'json'
                            ]
                        );
                    }
                    catch(Exception $e)
                    {
                        $twitter = null;
                    }
                    if (!empty($twitter)) {
                        $twitter_decoded = null;
                        $twitter_decoded = json_decode($twitter);
                        FileHandler::uploadFile(
                            json_encode($twitter_decoded), 
                            'twitter_' . \Auth::user()->id . '.snd', 
                            'social_media_data'
                        );
                    }
                }
                return redirect('live');
            }
            return redirect('twitter.error')
                ->with(
                    'flash_error', 
                    'Crab! Something went wrong while signing you up!'
                );
        }
    }

    /**
     * Manipulate Media Files for upload to Twitter
     *
     * @return \Illuminate\Http\Redirect
     */
    public function twitterMedia()
    {
        if (\Input::hasFile('upload')) {
            $file = \Input::file('upload');
            $extension = $file->getClientOriginalExtension();
            if (\Input::has('upload_rename')) {
                $name_rename = \Input::get('upload_rename') . '.' . $extension;
            } else {
                $name_rename = $file->getClientOriginalName();
            }
            $extension = $file->getClientOriginalExtension();
            \Storage::disk('local')
                ->makeDirectory(\Auth::user()->id);
            \Storage::disk('local')
                ->put(\Auth::user()->id . '/' . $name_rename, File::get($file));
            $uploaded_media = \Twitter::uploadMedia(
                [
                    'media' => \Storage::disk('local')
                        ->get(\Auth::user()->id . '/' . $name_rename) 
                ]
            );
            \Storage::disk('local')->delete(\Auth::user()->id . '/' . $name_rename);
            \Twitter::postTweet(
                [
                    'status' => \Input::get('tweetThis'), 
                    'media_ids' => $uploaded_media->media_id_string
                ]
            );
        } elseif (\Input::has('upload')) {
            $filename_whole = explode(
                'yoosapiusers/' 
                . \Auth::user()->id 
                . '/',
                \Input::get('upload')
            );
            $filename = explode('?X-Amz', $filename_whole[1]) [0];
            $file_upload = FileHandler::getFile($filename);
            $uploaded_media = \Twitter::uploadMedia(['media' => $file_upload]);
            \Twitter::postTweet(
                [
                    'status' => \Input::get('tweetThis'), 
                    'media_ids' => $uploaded_media->media_id_string
                ]
            );
        } else {
            \Twitter::postTweet(['status' => \Input::get('tweetThis') ]);
        }
        return redirect()
            ->back();
    }

    /**
     * Post Comment and Medit to Twitter
     *
     * @return \Illuminate\Http\Redirect
     */
    public function twitterBlaster()
    {
        if (\Input::hasFile('upload')) {
            $file = \Input::file('upload');
            $extension = $file->getClientOriginalExtension();
            if (\Input::has('upload_rename')) {
                $name_rename = \Input::get('upload_rename') . '.' . $extension;
            } else {
                $name_rename = $file->getClientOriginalName();
            }
            $extension = $file->getClientOriginalExtension();
            \Storage::disk('local')
                ->makeDirectory(\Auth::user()->id);
            \Storage::disk('local')
                ->put(\Auth::user()->id . '/' . $name_rename, File::get($file));
            $uploaded_media = \Twitter::uploadMedia(
                [
                    'media' => \Storage::disk('local')
                        ->get(\Auth::user()->id . '/' . $name_rename)
                ]
            );
            \Storage::disk('local')->delete(\Auth::user()->id . '/' . $name_rename);
            \Twitter::postTweet(
                [
                    'status' => \Input::get('tweetThis'), 
                    'media_ids' => $uploaded_media->media_id_string
                ]
            );
        } else {
            \Twitter::postTweet(['status' => \Input::get('content') ]);
        }
        
        return redirect()
            ->back();
    }

    /**
     * Get Login URL for connecting to Pinterest
     *
     * @return \Illuminate\Http\Redirect
     */
    public function pinterestLogin()
    {
        $pinterest = new Pinterest('oauth_key', 'oauth_secret');
        $loginUrlPin = $pinterest
            ->auth
            ->getLoginUrl(
                'https://'.$domain.'pinterestredirect', 
                array(
                    'read_public',
                    'write_public'
                )
            );

        return redirect($loginUrlPin);
    }

    /**
     * Get Redirect URL for connecting to Pinterest
     *
     * @return \Illuminate\Http\Redirect
     */
    public function pinterestRedirect()
    {
        $pinterest = new Pinterest('oauth_key', 'oauth_secret');
        $database_token = SocialToken::find(\Auth::user()->id);
        if ($database_token->pinterest_oauth_token) {
            $pinterest
                ->auth
                ->setOAuthToken($database_token->pinterest_oauth_token);
        } elseif (isset($_GET["code"])) {
            $token = $pinterest
                ->auth
                ->getOAuthToken($_GET["code"]);
            $pinterest
                ->auth
                ->setOAuthToken($token->access_token);
            $database_token->update(
                [
                    'pinterest_oauth_token' => $token->access_token
                ]
            );
        }
        $pins = $pinterest
            ->users
            ->getMePins(
                array(
                    'fields' => 'url,creator,note,media,attribution,image[original]'
                )
            );

        $pins_decode = json_decode($pins);

        FileHandler::uploadFile(
            json_encode($pins_decode), 
            'pinterest_' . \Auth::user()->id . '.snd', 
            'social_media_data'
        );

        return redirect('live');
    }
    
    /**
     * Manipulate Medit for upload to Pinterest
     *
     * @return \Illuminate\Http\Redirect
     */
    public function pinterestMedia()
    {
        $pinterestText = \Input::get('pinterestText');
        $pinterestFile = $_FILES['pinterestUpload']['tmp_name'];
        $pinterest = new Pinterest('oauth_key', 'oauth_secret');
        $database_token = SocialToken::find(\Auth::user()->id);
        if ($database_token->pinterest_oauth_token) {
            $pinterest
                ->auth
                ->setOAuthToken($database_token->pinterest_oauth_token);
        }
        $pin_boards = $pinterest
            ->users
            ->getMeBoards();
        $pin_boards = $pinterest
            ->users
            ->getMeBoards(['fields' => 'id,name,image,url']);
        $pin_board_url = $pin_boards{0}->url;
        $pin_board_name = explode('pinterest.com/', $pin_board_url);
        $pin_board_name_propper = substr($pin_board_name[1], 0, -1);

        $pinterest
            ->pins
            ->create(
                array(
                    "note" => $pinterestText,
                    "image" => $pinterestFile,
                    "board" => $pin_board_name_propper
                )
            );

        return redirect('live');
    }

    /**
     * Create Login URL for Reddit
     *
     * @return \Illuminate\Http\Redirect
     */
    public function redditConnect()
    {
        $ENDPOINT_STANDARD = 'http://www.reddit.com';
        $ENDPOINT_OAUTH = 'https://oauth.reddit.com';
        $ENDPOINT_OAUTH_AUTHORIZE = 'https://www.reddit.com/api/v1/authorize';
        $ENDPOINT_OAUTH_TOKEN = 'https://www.reddit.com/api/v1/access_token';
        $ENDPOINT_OAUTH_REDIRECT = 'https://yoosapi.net/redditredirect';
        $DURATION = 'permanent';
        $CLIENT_ID = 'oauth_id';
        $CLIENT_SECRET = 'oauth_secret';
        $SCOPES = 'save,modposts,identity,edit,flair,history';
        $SCOPES .= ',modconfig,modflair,modlog,modposts,modwiki,';
        $SCOPES .= 'mysubreddits,privatemessages,read,report,submit,';
        $SCOPES .= 'subscribe,vote,wikiedit,wikiread';
        $state = rand();
        $urlAuth = sprintf(
            "%s?client_id=%s&response_type=code&state=%s&redirect_uri=%s&duration=%s&scope=%s", 
            $ENDPOINT_OAUTH_AUTHORIZE, 
            $CLIENT_ID, 
            $state, 
            $ENDPOINT_OAUTH_REDIRECT, 
            $DURATION, 
            $SCOPES
        );

        return redirect($urlAuth);
    }

    /**
     * Create Redirect URL for Reddit
     *
     * @return \Illuminate\Http\Redirect
     */
    public function redditRedirect()
    {
        $ENDPOINT_STANDARD = 'http://www.reddit.com';
        $ENDPOINT_OAUTH = 'https://oauth.reddit.com';
        $ENDPOINT_OAUTH_AUTHORIZE = 'https://www.reddit.com/api/v1/authorize';
        $ENDPOINT_OAUTH_TOKEN = 'https://www.reddit.com/api/v1/access_token';
        $ENDPOINT_OAUTH_REDIRECT = 'https://yoosapi.net/redditredirect';
        $CLIENT_ID = 'oauth_client';
        $CLIENT_SECRET = 'oauth_secret';
        $SCOPES = 'save,modposts,identity,edit,flair,history';
        $SCOPES .= ',modconfig,modflair,modlog,modposts,modwiki,';
        $SCOPES .= 'mysubreddits,privatemessages,read,report,submit,';
        $SCOPES .= 'subscribe,vote,wikiedit,wikiread';

        $database_token = SocialToken::find(\Auth::user()->id);
        if ($database_token->reddit_oauth_refresh_token) {
            $client = new Client();
            $response = $client->request(
                'POST', 
                $ENDPOINT_OAUTH_TOKEN, 
                [
                    'form_params' => 
                        [
                            'grant_type' => 'refresh_token', 
                            'refresh_token' 
                            => $database_token->reddit_oauth_refresh_token
                        ],
                    'headers' => 
                        [
                            'User-Agent' => 'Yoosapi/1.0', 
                            'Authorization' 
                            => 'Basic '  . base64_encode(
                                $CLIENT_ID 
                                . ":" 
                                . $CLIENT_SECRET
                            ),
                            'Content-Type' => 'application/x-www-form-urlencoded'
                        ]
                ]
            );
            $response_token = json_decode($response->getBody());
            $access_token = $response_token->access_token;
            $token_type = $response_token->token_type;
            if (property_exists($response_token, 'refresh_token')) {
                $refresh_token = $response_token->refresh_token;
            } else {
                $refresh_token = $database_token->reddit_oauth_refresh_token;
            }
            $database_auth = SocialToken::find(\Auth::user()->id);
            $database_auth->update(
                [
                    'reddit_oauth_token' => $access_token, 
                    'reddit_oauth_token_type' => $token_type, 
                    'reddit_oauth_refresh_token' => $refresh_token
                ]
            );
        } else {

            $code = $_GET["code"];

            $client = new Client();
            $response = $client->request(
                'POST', 
                $ENDPOINT_OAUTH_TOKEN, 
                [
                    'form_params' => 
                        [
                            'grant_type' => 'authorization_code', 
                            'code' => $code, 
                            'redirect_uri' => $ENDPOINT_OAUTH_REDIRECT
                        ], 
                    'headers' => 
                        [
                            'User-Agent' => 'Current/1.0', 
                            'Authorization' => 'Basic ' 
                            . base64_encode(
                                $CLIENT_ID 
                                . ":" 
                                . $CLIENT_SECRET
                            ) 
                        ]
                ]
            );
            $response_token = json_decode($response->getBody());
            $access_token = $response_token->access_token;
            $token_type = $response_token->token_type;
            $refresh_token = $response_token->refresh_token;
            $database_auth = SocialToken::find(\Auth::user()->id);
            $database_auth->update(
                [
                    'reddit_oauth_token' => $access_token, 
                    'reddit_oauth_token_type' => $token_type, 
                    'reddit_oauth_refresh_token' => $refresh_token
                ]
            );
        }
        $client_user = new Client();
        $response_user = $client_user->request(
            'GET', 
            $ENDPOINT_OAUTH . "/api/v1/me", 
            [
                'headers' => 
                    [
                        'User-Agent' => 'Yoosapi/1.0', 
                        'Authorization' => $token_type . " " . $access_token
                    ]
            ]
        );

        $reddit_user = json_decode($response_user->getBody());
        FileHandler::uploadFile(
            json_encode($reddit_user), 
            'reddit_' . \Auth::user()->id . '.snd', 
            'social_media_data'
        );
        return redirect('live');

    }

    /**
     * Manipulate Reddit Media
     *
     * @return \Illuminate\Http\Redirect
     */
    public function redditMedia()
    {
        return redirect('live');
    }

    /**
     * Create Login URL to connect to Tumblr
     * 
     * @param object $request \Illuminate\Http\Request
     *
     * @return \Illuminate\Http\Redirect
     */
    public function tumblrConnect(Request $request)
    {
        $consumer_key = "oauth_key";
        $consumer_secret = "oauth_secret";
        $callback_url = "https://tumblrredirect";
        $tum_oauth = new \App\Tumblr\TumblrOAuth($consumer_key, $consumer_secret);
        $request_token = $tum_oauth->getRequestToken($callback_url);
        $token = $request_token['oauth_token'];
        $request->session()
            ->put('request_token', $token);
        $request->session()
            ->put('request_token_secret', $request_token['oauth_token_secret']);
        switch ($tum_oauth->http_code)
        {
        case 200:
                $url = $tum_oauth->getAuthorizeURL($token);
            return redirect($url);
            break;
        default:
            echo 'Could not connect to Tumblr. Refresh the page or try again later.';
        }
    }

    /**
     * Create Redirect URL to connect to Tumblr
     * 
     * @param object $request \Illuminate\Http\Request
     *
     * @return \Illuminate\Http\Redirect
     */
    public function tumblrRedirect(Request $request)
    {
        $consumer_key = "oauth_key";
        $consumer_secret = "oauth_secret";
        $database_token = SocialToken::find(\Auth::user()->id);
        if ($database_token->tumblr_oauth_token) {
            $oauth_token = $database_token->tumblr_oauth_token;
            $oauth_token_secret = $database_token->tumblr_oauth_token_secret;
        } else {
            $request_token = $request->session()
                ->get('request_token');
            $request_token_secret = $request->session()
                ->get('request_token_secret');
            $tum_oauth = new \App\Tumblr\TumblrOAuth(
                $consumer_key, 
                $consumer_secret, 
                $request_token, 
                $request_token_secret
            );
            $access_token = $tum_oauth->getAccessToken($_REQUEST['oauth_verifier']);

            if (200 == $tum_oauth->http_code) {
            } else {
                die('Unable to authenticate');
            }
            $oauth_token = $access_token['oauth_token'];
            $oauth_token_secret = $access_token['oauth_token_secret'];
            $database_token = SocialToken::find(\Auth::user()->id);
            $database_token->update(
                [
                    'tumblr_oauth_token' => $oauth_token, 
                    'tumblr_oauth_token_secret' => $oauth_token_secret
                ]
            );
        }
        $tumblr = new \App\Tumblr\TumblrOAuth(
            $consumer_key, 
            $consumer_secret, 
            $oauth_token, 
            $oauth_token_secret
        );
        $tumblr_array = array();
        $tumblr_array['tumblr_userinfo'] 
            = $tumblr->get('http://api.tumblr.com/v2/user/info');

        $tumblr_array['tumblr_screen_name'] 
            = $tumblr_array['tumblr_userinfo']
            ->response
            ->user->name;
        $user_dashboard = $tumblr->get('http://api.tumblr.com/v2/user/dashboard');
        $tumblr_array['tumblr_dashboard_posts'] = $user_dashboard
            ->response->posts;
        $tumblr_array['tumblr_user_following'] 
            = $tumblr->get('http://api.tumblr.com/v2/user/following');

        $blog_name = $tumblr_array['tumblr_userinfo']
            ->response
            ->user->blogs{0}->name;
        $tumblr_array['tumblr_own_blog_posts'] 
            = $tumblr->get(
                'http://api.tumblr.com/v2/blog/' 
                . $blog_name 
                . '/posts?api_key=' 
                . $consumer_key
            );

        FileHandler::uploadFile(
            serialize($tumblr_array), 
            'tumblr_' . \Auth::user()->id . '.snd', 
            'social_media_data'
        );

        return redirect('live');
    }

    /**
     * Manipulate Media to upload to Tumblr
     * 
     * @param object $request \Illuminate\Http\Request
     *
     * @return \Illuminate\Http\Redirect
     */
    public function tumblrMedia(Request $request)
    {
        $tumblrText = $request->get('tumblrText');
        $tumblrFile = $request->file('tumblrUpload');
        $tumblrFile = base64_encode(file_get_contents($tumblrFile));

        $consumer_key = "oauth_key";
        $consumer_secret = "oauth_secret";
        $database_token = SocialToken::find(\Auth::user()->id);
        if ($database_token->tumblr_oauth_token) {
            $oauth_token = $database_token->tumblr_oauth_token;
            $oauth_token_secret = $database_token->tumblr_oauth_token_secret;
        } else {
            $request_token = $request->session()
                ->get('request_token');
            $request_token_secret = $request->session()
                ->get('request_token_secret');
            $tum_oauth = new \App\Tumblr\TumblrOAuth(
                $consumer_key, 
                $consumer_secret, 
                $request_token, 
                $request_token_secret
            );
            $access_token = $tum_oauth->getAccessToken($_REQUEST['oauth_verifier']);

            if (200 == $tum_oauth->http_code) {
            } else {
                die('Unable to authenticate');
            }
            $oauth_token = $access_token['oauth_token'];
            $oauth_token_secret = $access_token['oauth_token_secret'];
            $database_token = SocialToken::find(\Auth::user()->id);
            $database_token->update(
                [
                    'tumblr_oauth_token' => $oauth_token, 
                    'tumblr_oauth_token_secret' => $oauth_token_secret
                ]
            );
        }
        $tumblr = new \App\Tumblr\TumblrOAuth(
            $consumer_key, 
            $consumer_secret, 
            $oauth_token, 
            $oauth_token_secret
        );
        $tumblr_own_blog_name = $tumblr->get('http://api.tumblr.com/v2/user/info')
            ->response
            ->user->blogs{0}->name;

        if (empty($tumblrFile)) {
            $tumblr_own_blog = $tumblr->post(
                'http://api.tumblr.com/v2/blog/' 
                . $tumblr_own_blog_name 
                . '/post', 
                ['type' => 'text', 'title' => $tumblrText]
            );
        } else {
            $tumblr_own_blog = $tumblr->post(
                'http://api.tumblr.com/v2/blog/' 
                . $tumblr_own_blog_name 
                . '/post', 
                [
                    'type' => 'photo', 
                    'title' => $tumblrText, 
                    'data64' => $tumblrFile
                ]
            );
        }

        return redirect('live');
    }

    /**
     * Create Login URL to connect to Instagram
     * 
     * @return \Illuminate\Http\Redirect
     */
    public function instagramConnect()
    {
        $instagram_url = 'https://api.instagram.com/oauth/authorize/';
        $instagram_url .= '?client_id=ef8bc51f311f4a508b0fee80d35f71c3';
        $instagram_url .= '&redirect_uri=https://instagramredirect&';
        $instagram_url .= 'response_type=code&scope=basic+';
        $instagram_url .= 'follower_list+public_content';

        return redirect()->to($instagram_url);
    }

    /**
     * Create Redirect URL to connect to Instagram
     * 
     * @return \Illuminate\Http\Redirect
     */
    public function instagramRedirect()
    {
        $client = new Client();
        $database_token = SocialToken::find(\Auth::user()->id);
        if ($database_token->instagram_oauth_token) {
            $access_token = $database_token->instagram_oauth_token;
        } else {
            define("CLIENT_ID", "oauth_id");
            define("CLIENT_SECRET", "oauth_secret");
            define("REDIRECT_URL", "https://instagramredirect");
            $code = $_GET['code'];
            $response = $client->post(
                'https://api.instagram.com/oauth/access_token', 
                array(
                    'form_params' => array(
                    'client_id' => CLIENT_ID,
                    'client_secret' => CLIENT_SECRET,
                    'grant_type' => 'authorization_code',
                    'redirect_uri' => REDIRECT_URL,
                    'code' => $code
                )
                )
            );
            $instagram = json_decode($response->getBody());
            $access_token = $instagram->access_token;
            $database_token = SocialToken::find(\Auth::user()->id);
            $database_token->update(['instagram_oauth_token' => $access_token]);
        }
        $instagram_array = array();
        $instagram_user_request 
            = $client->get(
                'https://api.instagram.com/v1/users/self/?access_token=' 
                . $access_token
            );
        $instagram_array['user'] = json_decode($instagram_user_request->getBody());
        $instagram_images_request  = $client->get(
            'https://api.instagram.com/v1/users/self/media/recent/?access_token='
            . $access_token
        );
        $instagram_array['images'] 
            = json_decode($instagram_images_request->getBody());
        FileHandler::uploadFile(
            serialize($instagram_array), 
            'instagram_' . \Auth::user()->id . '.snd', 
            'social_media_data'
        );
        
        return redirect('live');
    }

}
