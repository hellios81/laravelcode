<?php
/**
 * Working Hours Controller File Doc Comment
 * php version 7.*+
 * 
 * @category  Controller
 * @package   Controller_For_Working_Hours
 * @author    Srdjan Milicevic <webmaster@solveit.in.rs>
 * @copyright 2018 SolveIT d.o.o. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      https://solveit.in.rs
 */
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Workers;
use App\WorkingHours;
use App\OrderRoles;
use App\OrdersAndRoles;
use App\Locations;
use App\Orders;
use App\OrderLocation;
use App\MonthlyWorkHours;

/**
 * WorkingHoursController Class Doc Comment
 * 
 * Class used to interact with Working Hours
 * 
 * @category  Class
 * @package   ControllerRelatedToWorkingHours
 * @author    Srdjan Milicevic <webmaster@solveit.in.rs>
 * @copyright 2018 SolveIT d.o.o. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      https://solveit.in.rs
 * 
 * @since 1.0.1
 */
class WorkingHoursController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * 
     * @param object $request     \Illuminate\Htpp\Request
     * @param object $workermodel Model Instance for Query
     *
     * @return \Illuminate\Support\Facades\View
     */
    public function index(Request $request, Workers $workermodel)
    {
        //Deo koda u kojem cu pronaci 
        //ko je poslednji put svakom od radnika uneo vreme
        $workinghourscurrentuser = WorkingHours::where(
            'user_id', 
            \Auth::user()->id
        )   ->get()
            ->pluck('user_id', 'worker_id')
            ->toArray();
        $workinghoursall = WorkingHours::all()
            ->pluck('user_id', 'worker_id')
            ->toArray();
        if ($request->has('_token')) {
            $worker = $workermodel->newQuery();
            if ($request->has('name') 
                && !is_null($request->input('name'))
            ) {
                $worker->where('name', 'like', '%' . $request->input('name') . '%');
            }
            if ($request->has('surname') 
                && !is_null($request->input('surname'))
            ) {
                $worker->where(
                    'surname', 
                    'like', 
                    '%' . $request->input('surname') . '%'
                );
            }
            if ($request->has('jmbg') 
                && !is_null($request->input('jmbg'))
            ) {
                $worker->where(
                    'jmbg', 
                    'like', 
                    '%' . $request->input('jmbg') . '%'
                );
            }
            if ($request->has('id_card_no') 
                && !is_null($request->input('id_card_no'))
            ) {
                $worker->where(
                    'id_card_no', 
                    'like', 
                    '%' . $request->input('id_card_no') . '%'
                );
            }

            $workers = $worker->paginate(1)
                ->appends($request->all());
        } else {
            $workers = Workers::orderBy('id_card_no', 'asc')->paginate(1);
        }
        $roles = OrderRoles::all();
        $ordersandroles = OrdersAndRoles::get()
            ->pluck('role_id', 'working_hours')
            ->toArray();
        $orderlocations = OrderLocation::where('is_disabled', null)
            ->orderBy('location_id')
            ->get();
        foreach ($orderlocations as $orderlocation) {
            $locations[$orderlocation->id] 
                = Locations::find($orderlocation->location_id)
                ->location_name 
                . ' - ' 
                . Orders::find($orderlocation->order_id)->order_no 
                . ' - ' 
                . Orders::find($orderlocation->order_id)->order_description;
        }
        $working_hours = WorkingHours::where(
            'worker_id', 
            $workers->items() [0] ->id
        )    ->get()
            ->last();
        if ($working_hours) {
            $last_date = $working_hours->working_date;
            $last_order_location = $working_hours->orderlocation_id;
        } else {
            $last_date = null;
            $last_order_location = null;
        }
        $recorded_times = \App\RecordedTime::where(
            'employee_id', 
            $workers->first()->employee_id
        )   ->orderBy('date', 'asc')
            ->orderBy('type', 'asc')
            ->get();
        if ($recorded_times->first()) {
            $date = null;
        }
        $recorded_times_array = [];

        if ($recorded_times->first()) {
            foreach ($recorded_times as $recorded) {
                $recorded_array['entry_time'] = "Nema";
                $recorded_array['entry_date'] = "Nema";
                $recorded_array['exit_time'] = "Nema";
                $recorded_array['exit_date'] = "Nema";
                $recorded_array['location'] = $recorded->location;
                $worker_id = $workers->first()->id;
                $recorded_work_hours = WorkingHours::where('worker_id', $worker_id)
                    ->where('working_date', $recorded->date)
                    ->first();
                if ($recorded_work_hours) {
                    $recorded_array['recorded_time'] 
                        = $recorded_work_hours->working_hours;
                } else {
                    $recorded_array['recorded_time'] = 0;
                }
                if ($recorded->date == $date) {
                    if ($recorded->type == 'entry') {
                        $recorded_times_array[$date]['entry_time'] = $recorded->time;
                        $recorded_times_array[$date]['entry_date'] = $recorded->date;
                    }
                    if ($recorded->type == 'exit') {
                        $recorded_times_array[$date]['exit_time'] = $recorded->time;
                        $recorded_times_array[$date]['exit_date'] = $recorded->date;
                    }
                } else {
                    $date = $recorded->date;
                    if ($recorded->type == 'entry') {
                        $recorded_array['entry_time'] = $recorded->time;
                        $recorded_array['entry_date'] = $recorded->date;
                    }
                    if ($recorded->type == 'exit') {
                        $recorded_array['exit_time'] = $recorded->time;
                        $recorded_array['exit_date'] = $recorded->date;
                    }

                    $recorded_times_array[$recorded->date] = $recorded_array;
                }
            }
        }

        if (count($recorded_times_array) > 0) {
            foreach ($recorded_times_array as $key => $recorded_time) {
                if ($recorded_time['entry_time'] > $recorded_time['exit_time']) {
                    $key_back = \Carbon\Carbon::createFromFormat('Y-m-d', $key)
                        ->subDays(1)
                        ->format('Y-m-d');
                    if (array_key_exists($key_back, $recorded_times_array)) {
                        if ($recorded_time['exit_time'] !== 'Nema' 
                            && $recorded_time['exit_date'] !== 'Nema'
                        ) {
                            $recorded_times_array[$key_back]['exit_time'] 
                                = $recorded_time['exit_time'];
                            $recorded_times_array[$key_back]['exit_date'] 
                                = $recorded_time['exit_date'];
                        }
                    }
                }
            }
            foreach ($recorded_times_array as $key => $recorded_time) {
                $key_back = \Carbon\Carbon::createFromFormat(
                    'Y-m-d', 
                    $key
                )->subDays(1)
                    ->format('Y-m-d');
                if (array_key_exists($key_back, $recorded_times_array)) {
                    if ($recorded_time['exit_time'] == 'Nema' 
                        && $recorded_time['exit_date'] == 'Nema'
                    ) {
                        if ($recorded_times_array[$key_back]['exit_time'] < $recorded_times_array[$key_back]['entry_time']
                        ) {
                            $recorded_times_array[$key_back]['exit_time'] 
                                = $recorded_time['entry_time'];
                            $recorded_times_array[$key_back]['exit_date'] 
                                = $recorded_time['entry_date'];
                        }
                        unset($recorded_times_array[$key]);
                    }
                }
            }
        }

        return view('workinghours.workinghours')->with(
            compact(
                'recorded_times_array', 
                'workers', 
                'roles', 
                'ordersandroles', 
                'locations', 
                'last_date', 
                'last_order_location'
            )
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request Request
     * 
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $message = [
            'required' => 'Neophodan podatak!', 
            'unique' => 'Podatak već postoji u bazi!', 
            'min' => 'Mora imati najmanje :min znakova!'
        ];
        if (array_key_exists('titleday', $data) 
            || array_key_exists('sick', $data) 
            || array_key_exists('nationalholiday', $data) 
            || array_key_exists('vacation', $data)
        ) {
            $validator = Validator::make(
                $request->all(), 
                ['role_id' => 'required', ], 
                $message
            );
        } else {
            $validator = Validator::make(
                $request->all(), 
                [
                    'working_hours' => 'required', 
                    'role_id' => 'required', 
                ], 
                $message
            );
        }
        if ($validator->fails()
        ) {
            return redirect()
                ->back()
                ->withErrors($validator)->withInput();
        }
        if (is_null($data['working_hours']) 
            && ($data['titleday'] == 1 
            || $data['sick'] == 1 
            || $data['nationalholiday'] == 1 
            || $data['vacation'] == 1)
        ) { 
            $data['working_hours'] = 8;
        }
        $worker = Workers::find($data['worker_id']);
        $worker->user_id = \Auth::user()->id;
        $worker->save();
        $data_date = \Carbon\Carbon::createFromFormat(
            'Y-m-d', 
            $data['working_date']
        );
        if ($data_date->dayOfWeek == 0) {
            $data['sunday'] = 1;
        } else {
            $data['sunday'] = 0;
        }
        $current_test_workhours 
            = WorkingHours::where('worker_id', $request->input('worker_id'))
            ->where('working_date', $request->input('working_date'))
            ->first();
        if (!is_null($current_test_workhours)) {
            $data['is_changed'] = 1;
        }
        if (is_null($current_test_workhours)) {
            $current_workhours = [
                'regular' => 0, 
                'overtime' => 0, 
                'sunday' => 0, 
                'night' => 0, 
                'sundaynight' => 0, 
                'hardened5' => 0, 
                'hardened26' => 0, 
                'sick' => 0, 
                'vacation' => 0, 
                'nationalholiday' => 0, 
                'titleday' => 0
            ];
        } else {
            $current_workhours = [
                'regular' => 0, 
                'overtime' => 0, 
                'sunday' => 0, 
                'night' => 0, 
                'sundaynight' => 0, 
                'hardened5' => 0, 
                'hardened26' => 0, 
                'sick' => 0, 
                'vacation' => 0, 
                'nationalholiday' => 0, 
                'titleday' => 0
            ];
            if ($current_test_workhours->titleday == 1) {
                $current_workhours['titleday'] = 8;
            } elseif ($current_test_workhours->sick == 1) {
                $current_workhours['sick'] = 8;
            } elseif ($current_test_workhours->nationalholiday == 1) {
                $current_workhours['nationalholiday'] = 8;
            } elseif ($current_test_workhours->vacation == 1) {
                $current_workhours['vacation'] = 8;
            } elseif ($current_test_workhours->night == 1 
                && $current_test_workhours->sunday == 1
            ) {
                $current_workhours['sundaynight'] 
                    = $current_test_workhours->working_hours;
            } elseif ($current_test_workhours->sunday == 1) {
                $current_workhours['sunday'] 
                    = $current_test_workhours->working_hours;
            } elseif ($current_test_workhours->night == 1) {
                $current_workhours['night'] 
                    = $current_test_workhours->working_hours;
            } elseif ($current_test_workhours->hardened5 == 1) {
                $current_workhours['hardened5'] 
                    = $current_test_workhours->working_hours;
            } elseif ($current_test_workhours->hardened26 == 1) {
                $current_workhours['hardened26'] 
                    = $current_test_workhours->working_hours;
            } else {
                if ($current_test_workhours->working_hours > 8) {
                    $current_workhours['regular'] = 8;
                    $current_workhours['overtime'] 
                        = $current_test_workhours->working_hours - 8;
                } else {
                    $current_workhours['regular'] 
                        = $current_test_workhours->working_hours;
                }
            }
        }
        $orderlocation = OrderLocation::find($data['orderlocation_id']);
        $order_id = $orderlocation->order_id;
        if (is_null($current_test_workhours)) {
            $current_workhours_sum = 0;
        } else {
            $current_workhours_sum = $current_test_workhours->working_hours;
        }
        $remaining_hours = OrdersAndRoles::where('order_id', $order_id)
            ->where('role_id', $data['role_id'])->first();
        if ($remaining_hours) {
            $remaining_hours->spent_working_hours 
                += $data['working_hours'] - $current_workhours_sum;
            $remaining_hours->save();
        }
        $workhours = WorkingHours::updateOrCreate(
            [
                'worker_id' => $request->input('worker_id'), 
                'working_date' => $request->input('working_date') 
            ], 
            $data
        );
        if (!is_null($request->input('route_parameter'))) {
            $explode = explode('=', $request->input('route_parameter'));
            if (is_array($explode) 
                && is_numeric($explode[1])
            ) {
                $route_parameter = $explode[1] + 1;
            } else {
                $route_parameter = 0;
            }
        } else {
            $route_parameter = 0;
        }
        $month = \Carbon\Carbon::createFromFormat(
            'Y-m-d', 
            $workhours->working_date
        )->month;
        $year = \Carbon\Carbon::createFromFormat(
            'Y-m-d', 
            $workhours->working_date
        )->year;
        //Monthly Working Hours
        $regular = 0;
        $overtime = 0;
        $sunday = 0;
        $night = 0;
        $sundaynight = 0;
        $hardened5 = 0;
        $hardened26 = 0;
        $sick = 0;
        $vacation = 0;
        $nationalholiday = 0;
        $titleday = 0;
        if ($workhours->titleday == 1) {
            $titleday = 8;
        } elseif ($workhours->sick == 1) {
            $sick = 8;
        } elseif ($workhours->nationalholiday == 1) {
            $nationalholiday = 8;
        } elseif ($workhours->vacation == 1) {
            $vacation = 8;
        } elseif ($workhours->night == 1 && $workhours->sunday == 1) {
            $sundaynight = $workhours->working_hours;
        } elseif ($workhours->sunday == 1) {
            $sunday = $workhours->working_hours;
        } elseif ($workhours->night == 1) {
            $night = $workhours->working_hours;
        } elseif ($workhours->hardened5 == 1) {
            $hardened5 = $workhours->working_hours;
        } elseif ($workhours->hardened26 == 1) {
            $hardened26 = $workhours->working_hours;
        } else {
            if ($workhours->working_hours > 8) {
                $regular = 8;
                $overtime = $workhours->working_hours - 8;
            } else {
                $regular = $workhours->working_hours;
            }
        }
        $work_hours_2 = WorkingHours::where('worker_id', $workhours->worker_id)
            ->where(
                'working_date', 
                'like', 
                $year . '-' . sprintf('%02d', $month) . '%'
            )->get();
        $regular2 = 0;
        $overtime2 = 0;
        $sunday2 = 0;
        $night2 = 0;
        $sundaynight2 = 0;
        $hardened52 = 0;
        $hardened262 = 0;
        $sick2 = 0;
        $vacation2 = 0;
        $nationalholiday2 = 0;
        $titleday2 = 0;
        foreach ($work_hours_2 as $work_hour_2) {
            if ($work_hour_2->titleday == 1) {
                $titleday2 += 8;
            } elseif ($work_hour_2->sick == 1) {
                $sick2 += 8;
            } elseif ($work_hour_2->nationalholiday == 1) {
                $nationalholiday2 += 8;
            } elseif ($work_hour_2->vacation == 1) {
                $vacation2 += 8;
            } elseif ($work_hour_2->night == 1 && $work_hour_2->sunday == 1) {
                $sundaynight2 += $work_hour_2->working_hours;
            } elseif ($work_hour_2->sunday == 1) {
                $sunday2 += $work_hour_2->working_hours;
            } elseif ($work_hour_2->night == 1) {
                $night2 += $work_hour_2->working_hours;
            } elseif ($work_hour_2->hardened5 == 1) {
                $hardened52 += $work_hour_2->working_hours;
            } elseif ($work_hour_2->hardened26 == 1) {
                $hardened262 += $work_hour_2->working_hours;
            } else {
                if ($work_hour_2->working_hours > 8) {
                    $regular2 += 8;
                    $overtime2 += $work_hour_2->working_hours - 8;
                } else {
                    $regular2 += $work_hour_2->working_hours;
                }
            }

        }
        $workhours_array = [
            'regular' => $regular2, 
            'overtime' => $overtime2, 
            'sunday' => $sunday2, 
            'night' => $night2, 
            'sundaynight' => $sundaynight2, 
            'hardened5' => $hardened52, 
            'hardened26' => $hardened262, 
            'sick' => $sick2, 
            'vacation' => $vacation2, 
            'nationalholiday' => $nationalholiday2, 
            'titleday' => $titleday2, 
            'workers_id' => $workhours->worker_id, 
            'month' => $month, 
            'year' => $year, 
            'last_date' => $workhours->working_date
        ];
        MonthlyWorkHours::updateOrCreate(
            [
                'workers_id' => $workhours->worker_id, 
                'month' => $month, 
                'year' => $year
            ], 
            $workhours_array
        );
        //End Monthly Working Hours
        if ($route_parameter == null) {
            if (array_key_exists('same_worker', $data)) {
                \Session::put('same_worker', $data['same_worker']);
            }
            return redirect()->back();
        } else {
            if (Workers::find($route_parameter)) {
                if (array_key_exists('same_worker', $data)) {
                    \Session::put('same_worker', $data['same_worker']);
                    return redirect()->to(
                        '/workinghours?page=' 
                        . ($route_parameter - 1)
                    );
                } else {
                    return redirect()->to('/workinghours?page=' . $route_parameter);
                }
            } else {
                return redirect()->to(
                    '/workinghours?page=' 
                    . ($route_parameter - 1)
                );
            }
        }
    }
    
}

