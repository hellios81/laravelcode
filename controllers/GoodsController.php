<?php
/**
 * Goods Controller File Doc Comment
 * php version 7.*+
 * 
 * @category  Controller
 * @package   Controller_For_Goods
 * @author    Srdjan Milicevic <webmaster@solveit.in.rs>
 * @copyright 2017 Kadet d.o.o. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      https://solveit.in.rs
 */
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Stock;
use App\Goods;
use App\GoodsCategories;
use App\GoodsOptions;
use App\Options;
use App\OptionsCategories;
use App\Artikli;
use App\Manufacturer;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Response;
use Validator;

/**
 * GoodsController Class Doc Comment
 * 
 * Class used to interact with Goods
 * 
 * @category  Class
 * @package   ControllerRelatedForGoods
 * @author    Srdjan Milicevic <webmaster@solveit.in.rs>
 * @copyright 2017 Kadet d.o.o. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      https://solveit.in.rs
 * 
 * @since 1.0.1
 */
class GoodsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Search Goods
     *
     * @param object $request        Laravel Request object
     * @param object $good           Model Goods
     * @param string $sort_parameter 'asc','desc',columns
     * 
     * @return \Illuminate\Http\Response
     */
    public function searchGoods(
        Request $request, 
        Goods $good, 
        $sort_parameter = null
    ) {
        $goods_options = [];
        $goods = $good->newQuery();
        if ($request->has('search_name')&&!is_null($request->input('search_name'))) {
            $search_code = explode('|', $request->input('search_name'))[0];
            $goods->where('code', $search_code);
        }
        if ($request->has('search_suplier_name')
            && !is_null($request->input('search_suplier_name'))
        ) {
            $explode = explode('|', $request->input('search_suplier_name'));
            $search_suplier_name = $explode[1];
            $search_suplier_code = $explode[0];
            if ($search_suplier_code) {
                $goods->where('suplier_code', $search_suplier_code);
            }
            if ($search_suplier_name) {
                $goods->where('suplier_name', $search_suplier_name);
            }
        }
        if ($request->has('search_manufacturers_id') 
            && $request->input('search_manufacturers_id')!=0
        ) {
            $goods->where(
                'manufacturers_id', 
                $request->input('search_manufacturers_id')
            );
        }
        if ($request->has('search_category') 
            && $request->input('search_category')!=0
        ) {
            $goods->where('category_id', $request->input('search_category'));
        }
        $sort = session('sort');
        if ($sort == 'asc') {
            $sort = 'desc';
            session(['sort'=>'desc']);
        } elseif ($sort == 'desc') {
            $sort = 'asc';
            session(['sort'=>'asc']);
        }
        if ($sort_parameter == null) {
            $sort_parameter = 'code';
            $sort = 'asc';
            session(['sort'=>'asc']);
        }
        if ($sort_parameter !== 'filters'
            || $request->has('search_name')
            && !is_null($request->input('search_name'))
            || $request->has('search_manufacturers_id') 
            && $request->input('search_manufacturers_id')!=0
            || $request->has('search_category') 
            && $request->input('search_category')!=0
        ) {
            $goods = $goods->orderBy('problem', 'asc')
                ->orderBy('imported', 'asc')
                ->orderBy($sort_parameter, $sort)
                ->paginate(50)
                ->appends($request->except('page'));
        } else {
            if ($sort_parameter == 'filters') {
                $goods_options_array = GoodsOptions::groupBy('goods_id')
                    ->pluck('goods_id', 'goods_id')->toArray();
                $goods = Goods::whereNotIn('id', $goods_options_array)
                    ->orderBy('problem', 'asc')
                    ->orderBy('imported', 'asc')
                    ->where('problem', 0)
                    ->orderBy('category_id', 'asc')
                    ->orderBy('manufacturers_id', 'asc')
                    ->orderBy('code', 'asc')->paginate(50);
            } else {
                $goods = Goods::orderBy($sort_parameter, $sort)
                    ->orderBy('problem', 'asc')
                    ->orderBy('imported', 'asc')
                    ->paginate(50)
                    ->appends($request->except('page'));
            }
        }
        $goods_options_primary = Options::get();
        foreach ($goods_options_primary as $goods_option_primary) {
            $goods_options[$goods_option_primary->id] 
                = $goods_option_primary->optionscategories->option_category_name
                . " - "
                . $goods_option_primary->option_name;
        }
        $options_array = GoodsOptions::orderBy('goods_id', 'asc')->get();
        if ($options_array->first()) {
            $test_option = $options_array->first()->goods_id;
        } else {
            $test_option = null;
        }
        $options = [];
        foreach ($options_array as $key => $option_single) {
            if ($option_single->goods_id == $test_option) {
                $option[] = $option_single->options_id;
            } else {
                $test_option = $option_single->goods_id;
                $option[] = $option_single->options_id;
                if (isset($option) && is_array($option)) {
                    $options[$option_single->goods_id] = $option;
                    unset($option);
                }
            }
        }
        if (isset($option)) {
            $options[$test_option] = $option;
        }
        $parent_categories_id = GoodsCategories::pluck('parent_category_id')->all();
        $parent_categories_id = array_values(array_unique($parent_categories_id));
        $categories = GoodsCategories::whereNotIn('id', $parent_categories_id)
            ->where('parent_category_id', '<>', 0)->get();
        $focus = null;
        $manufacturers = Manufacturer::orderBy('name', 'asc')->get();

        return view('goods')->with(
            compact(
                'goods',
                'options',
                'goods_options',
                'categories',
                'focus',
                'manufacturers'
            )
        );
    }

    /**
     * Show Goods All.
     *
     * @param object $request        Laravel Request object
     * @param object $good           Goods
     * @param string $sort_parameter 'asc','desc',columnds
     * 
     * @return \Illuminate\Http\Response
     */
    public function goods(Request $request, Goods $good, $sort_parameter = null)
    {
        $goods_options = [];
        $goods = $good->newQuery();
        if ($request->has('search_name')&&!is_null($request->input('search_name'))) {
            $search_code = explode('|', $request->input('search_name'))[0];
            $goods->where('code', $search_code);
        }
        if ($request->has('search_suplier_name')
            && !is_null($request->input('search_suplier_name'))
        ) {
            $explode = explode('|', $request->input('search_suplier_name'));
            $search_suplier_name = $explode[1];
            $search_suplier_code = $explode[0];
            if ($search_suplier_code) {
                $goods->where('suplier_code', $search_suplier_code);
            }
            if ($search_suplier_name) {
                $goods->where('suplier_name', $search_suplier_name);
            }
        }
        if ($request->has('search_manufacturers_id') 
            && $request->input('search_manufacturers_id')!=0
        ) {
            $goods->where(
                'manufacturers_id', 
                $request->input('search_manufacturers_id')
            );
        }
        if ($request->has('search_category') 
            && $request->input('search_category')!=0
        ) {
            $goods->where('category_id', $request->input('search_category'));
        }
        if (\Auth::user()->role == 1) {
            return redirect()->back()->with(
                [
                    'message' => 'Nemate dozvolu za pristup!',
                    'message_type' => 'danger'
                ]
            );
        }
        $sort = session('sort');
        if ($sort == 'asc') {
            $sort = 'desc';
            session(['sort'=>'desc']);
        } elseif ($sort == 'desc') {
            $sort = 'asc';
            session(['sort'=>'asc']);
        }
        if ($sort_parameter == null) {
            $sort_parameter = 'code';
            $sort = 'asc';
            session(['sort'=>'asc']);
        }
        if ($sort_parameter !== 'filters'
            || $request->has('search_name')
            && !is_null($request->input('search_name'))
            || $request->has('search_manufacturers_id') 
            && $request->input('search_manufacturers_id')!=0
            || $request->has('search_category') 
            && $request->input('search_category')!=0
        ) {
            $goods = $goods->orderBy('problem', 'asc')
                ->orderBy('imported', 'asc')
                ->orderBy($sort_parameter, $sort)
                ->paginate(50)
                ->appends($request->except('page'));
        } else {
            if ($sort_parameter == 'filters') {
                $goods_options_array = GoodsOptions::groupBy('goods_id')
                    ->pluck('goods_id', 'goods_id')->toArray();
                $goods = Goods::whereNotIn('id', $goods_options_array)
                    ->where('problem', 0)
                    ->orderBy('problem', 'asc')
                    ->orderBy('imported', 'asc')
                    ->orderBy('category_id', 'asc')
                    ->orderBy('manufacturers_id', 'asc')
                    ->orderBy('code', 'asc')
                    ->paginate(50);
            } else {
                $goods = Goods::orderBy($sort_parameter, $sort)
                    ->orderBy('problem', 'asc')
                    ->orderBy('imported', 'asc')
                    ->paginate(50)
                    ->appends($request->except('page'));
            }
        }
        $goods_options_primary = Options::get();
        foreach ($goods_options_primary as $goods_option_primary) {
            $goods_options[$goods_option_primary->id] 
                = $goods_option_primary->optionscategories->option_category_name
                . " - "
                . $goods_option_primary->option_name;
        }
        $options_array = GoodsOptions::orderBy('goods_id', 'asc')->get();
        if ($options_array->first()) {
            $test_option = $options_array->first()->goods_id;
        } else {
            $test_option = null;
        }
        $options = [];
        foreach ($options_array as $key => $option_single) {
            if ($option_single->goods_id == $test_option) {
                $option[] = $option_single->options_id;
            } else {
                $test_option = $option_single->goods_id;
                $option[] = $option_single->options_id;
                if (isset($option) && is_array($option)) {
                    $options[$option_single->goods_id] = $option;
                    unset($option);
                }
            }
        }
        if (isset($option)) {
            $options[$test_option] = $option;
        }
        $parent_categories_id = GoodsCategories::pluck('parent_category_id')->all();
        $parent_categories_id = array_values(array_unique($parent_categories_id));
        $categories = GoodsCategories::whereNotIn('id', $parent_categories_id)
            ->where('parent_category_id', '<>', 0)->get();
        $focus = null;
        $manufacturers = Manufacturer::orderBy('name', 'asc')->get();

        return view('goods')->with(
            compact(
                'goods',
                'options',
                'goods_options',
                'categories',
                'focus',
                'manufacturers'
            )
        );
    }

    /**
     * Save Goods
     *
     * @param object $request Laravel Request object
     * 
     * @return \Illuminate\Http\Response
     */
    public function saveGood(Request $request)
    {
        $data = $request->all();
        array_shift($data);
        $good_id = array_shift($data);
        $button = $data['button'];
        $data['slug'] = str_slug($data['name']);
        unset($data['button']);
        if ($button == 'newsave') {
            $message = [
                'required' => 'Neophodan podatak!',
                'unique' => 'Podatak već postoji u bazi!',
                'min' => 'Mora imati najmanje :min znakova!'
            ];
            $validator = Validator::make(
                $request->all(), 
                [
                    'name' => 'required|unique:goods',
                    'code' => 'required|unique:goods',
                    'price' => 'required',
                    'suplier_price' => 'required',
                    'suplier_code' => 'required',
                    'suplier_name' => 'required',
                ], 
                $message
            );
            if ($validator->fails()) {
                return redirect('/goods')
                    ->withErrors($validator)
                    ->withInput();
            }
            Goods::create($data);
            session(['focus'=>$good_id]);
        }
        if ($button == 'save') {
            Goods::updateOrCreate(['id' => $good_id], $data);
            session(['focus'=>$good_id]);
        } elseif ($button == 'delete') {
            if (\Auth::user()->id !== 1) {
                return redirect()->back()->with(
                    [
                        'message'=>'Nemate dozvolu za pristup!', 
                        'message_type' => 'danger'
                    ]
                );
            }

            $good = Goods::find($good_id);
            if (Stock::where('goods_id', $good_id)->exists()) {
                if ($request->method()=='POST') {
                    return redirect()->to('goods')->with(
                        [
                            'message' 
                        => 
                        'Nije dozvoljeno da obrišete artikal! Postoji na stanju!',
                            'message_type' => 'danger'
                        ]
                    );
                } else {
                    return redirect()->back()->with(
                        [
                            'message' 
                        => 
                        'Nije dozvoljeno da obrišete artikal! Postoji na stanju!',
                            'message_type' => 'danger'
                        ]
                    );
                }
            } else {
                if (!empty($good->image) 
                    && file_exists(public_path('/images/goods/'.$good->image))
                ) {
                    unlink(public_path('/images/goods/'.$good->image));
                }
                Goods::destroy($good_id);
            }
            
            return redirect()->back();
        }
    }

    /**
     * Mark Goods that have a problem and should not be displayed to Front End Store
     *
     * @param object $request Laravel Request object
     * 
     * @return \Illuminate\Http\Response
     */
    public function problemGood(Request $request)
    {
        $good_id = $request->input('good_id');
        $good = Goods::find($good_id);
        if ($request->has('problem')) {
            $good->problem = 1;
        } elseif ($request->has('noproblem')) {
            $good->problem = 0;
        } elseif ($request->has('noimported')) {
            $good->imported = 0;
        }
        session(['focus'=>$good_id]);
        $good->save();
        
        return redirect()->back();
    }

    /**
     * Show categories of Goods.
     *
     * @return \Illuminate\Http\Response
     */
    public function categories()
    {
        $parent_categories_id = GoodsCategories::pluck('parent_category_id')->all();
        $parent_categories_id = array_values(array_unique($parent_categories_id));
        $categories = GoodsCategories::whereNotIn('id', $parent_categories_id)
            ->where('parent_category_id', '<>', 0)
            ->orderBy('parent_category_id', 'asc')->get();
        $parentcategories = GoodsCategories::where('parent_category_id', '0')
            ->orderBy('id', 'asc')->get();
        $icons = [
            'flaticon-air-conditioning',
            'flaticon-alarm-clock',
            'flaticon-blender',
            'flaticon-blender-1',
            'flaticon-blender-2',
            'flaticon-coffee-maker',
            'flaticon-curler',
            'flaticon-dishwasher',
            'flaticon-epilator',
            'flaticon-extractor',
            'flaticon-fan',
            'flaticon-food-steamer',
            'flaticon-fridge',
            'flaticon-hair-clipper',
            'flaticon-hairdryer',
            'flaticon-iron',
            'flaticon-juicer',
            'flaticon-kettle',
            'flaticon-lamp',
            'flaticon-meat-grinder',
            'flaticon-microwave-oven',
            'flaticon-mixer',
            'flaticon-mixer-1',
            'flaticon-multicooker',
            'flaticon-music-player',
            'flaticon-music-speaker',
            'flaticon-oven',
            'flaticon-photo-camera',
            'flaticon-printer',
            'flaticon-refrigerator',
            'flaticon-sewing-machine',
            'flaticon-shaver',
            'flaticon-stove',
            'flaticon-telephone',
            'flaticon-television',
            'flaticon-toaster',
            'flaticon-toothbrush',
            'flaticon-vacuum-cleaner',
            'flaticon-vacuum-cleaner-1',
            'flaticon-washing-machine',
        ];

        return view('categories')->with(
            compact(
                'categories',
                'parentcategories',
                'icons'
            )
        );
    }

    /**
     * Save new Category of Goods
     *
     * @param object $request Laravel Request object
     * 
     * @return \Illuminate\Http\Response
     */
    public function saveCategory(Request $request)
    {
        $message = [
            'required' => 'Neophodan podatak!',
            'unique' => 'Podatak već postoji u bazi!',
            'min' => 'Mora imati najmanje :min znakova!'
        ];
        $validator = Validator::make(
            $request->all(), 
            [
                'category_name' => 'required|unique:goods_categories',
            ], 
            $message
        );
        if ($validator->fails()) {
            return redirect('/categories')
                ->withErrors($validator)
                ->withInput();
        }
        $category_name = $request->input('category_name');
        $parent_category_id = $request->input('parent_category_id');
        if ($parent_category_id == "0") {
            $parent_category = new GoodsCategories;
            $parent_category->category_name = $category_name;
            $parent_category->parent_category_id = 0;
            $parent_category->slug = str_slug($category_name);
            $parent_category->save();
        } else {
            $category = new GoodsCategories;
            $category->category_name = $category_name;
            $category->parent_category_id = $parent_category_id;
            $category->slug = str_slug($category_name);
            $category->save();
        }
        if ($parent_category_id) {
            session(['parent_category_id'=>$parent_category_id]);
        }
        
        return redirect()->back();
    }

    /**
     * Edit existing Category of Goods
     *
     * @param object $request Laravel Request object
     * 
     * @return \Illuminate\Http\Response
     */
    public function editCategory(Request $request)
    {
        if ($request->has('save')) {
            $category_id = $request->input('category_id');
        
            $category = GoodsCategories::find($category_id);
            if (!is_null($request->input('parent_category_id'))) {
                $category->parent_category_id 
                    = $request->input('parent_category_id');
            }
            if (!is_null($request->input('category_name'))) {
                $category->category_name = $request->input('category_name');
            }
            $category->icon = $request->input('icon');
            $category->save();
        } elseif ($request->has('delete')) {
            if (\Auth::user()->id !== 1) {
                return redirect()->back()->with(
                    [
                        'message'=>'Nemate dozvolu za pristup!', 
                        'message_type' => 'danger'
                    ]
                );
            }
            $category_id = $request->input('category_id');
            $category = GoodsCategories::find($category_id);
            $is_parent_category = GoodsCategories::where(
                'parent_category_id', 
                $category_id
            )->get();
            if ($is_parent_category->first()) {
                return redirect()->back()->with(
                    [
                        'message' 
                        => 
                        'Ne možete da brišete kategoriju koja ima podkategorije!', 
                        'message_type' => 'danger'
                    ]
                );
            }
            $categorized_goods = Goods::where('category_id', $category_id)->get();
            if ($categorized_goods->first()) {
                return redirect()->back()->with(
                    [
                        'message' 
                      => 
                      'Ne možete da brišete kategoriju koja ima proizvode u sebi!',
                      'message_type'=>'danger'
                    ]
                );
            }
            GoodsCategories::destroy($category_id);
        }

        return redirect()->back();
    }

    /**
     * Edit Category that has Child Categories
     *
     * @param object $request Laravel Request object
     * 
     * @return \Illuminate\Http\Response
     */
    public function editParentCategory(Request $request)
    {
        if ($request->has('save')) {
            $parent_category_id = $request->input('parent_category_id');
            $category = GoodsCategories::find($parent_category_id);
            $category->icon = $request->input('icon');
            if (!is_null($request->input('parent_category_id'))) {
                $category->parent_category_id 
                    = $request->input('parent_category_id');
            }
            if (!is_null($request->input('parent_category_name'))) {
                $category->category_name = $request->input('parent_category_name');
            }
            $category->save();
        } elseif ($request->has('delete')) {
            if (\Auth::user()->id !== 1) {
                return redirect()->back()->with(
                    [
                        'message' => 'Nemate dozvolu za pristup!',
                        'message_type' => 'danger'
                    ]
                );
            }
            $parent_category_id = $request->input('parent_category_id');
            $category = GoodsCategories::where('id', $parent_category_id)->get();
            $is_parent_category 
                = GoodsCategories::where(
                    'parent_category_id', 
                    $parent_category_id
                )->get();
            if ($is_parent_category->first()) {
                return redirect()->back()->with(
                    [
                        'message' 
                        => 
                        'Ne možete da brišete kategoriju koja ima podkategorije!',
                        'message_type' => 'danger'
                    ]
                );
            }
            $categorized_goods = Goods::where(
                'category_id', 
                $parent_category_id
            )->get();
            if ($categorized_goods->first()) {
                return redirect()->back()->with(
                    [
                        'message'
                        =>
                      'Ne možete da brišete kategoriju koja ima proizvode u sebi!',
                        'message_type' => 'danger'
                    ]
                );
            }
            GoodsCategories::destroy($parent_category_id);
        }

        return redirect()->back();
    }

    /**
     * Change Goods Category
     *
     * @param object $request Laravel Request object
     * 
     * @return \Illuminate\Http\Response
     */
    public function changeCategory(Request $request)
    {
        $category_id = $request->input('category_id');
        $goods_id = $request->input('goods_id');
        $good = Goods::find($goods_id);
        $good->category_id = $category_id;
        $good->save();
        session(['focus'=>$goods_id]);

        return redirect()->back();

    }

    /**
     * Get Description of a Good through AJAX
     *
     * @param object $request Laravel Request object
     * 
     * @return \Illuminate\Http\Response
     */
    public function getDescription(Request $request)
    {
        if ($request->isXmlHttpRequest()) {
            $good_id = $request->input('goods_id');
            $description = Goods::find($good_id)->description;
            
            return json_encode(
                [
                    'status' => 'success',
                    'description' => $description
                ]
            );
        }
    }

    /**
     * Show Options for all Goods
     *
     * @return \Illuminate\Http\View
     */
    public function options()
    {
        $options = Options::orderBy('options_categories_id', 'asc')->get();
        $optionscategories = OptionsCategories::all();

        return view('options')->with(
            compact(
                'options',
                'optionscategories'
            )
        );
    }

    /**
     * Save newly created option for goods
     *
     * @param object $request Laravel Request object
     * 
     * @return \Illuminate\Http\Response
     */
    public function saveOption(Request $request)
    {
        $message = [
            'required' => 'Neophodan podatak!',
            'unique' => 'Podatak već postoji u bazi!',
            'min' => 'Mora imati najmanje :min znakova!'
        ];
        $validator = Validator::make(
            $request->all(), 
            [
                'option_name' => 'required',
            ], 
            $message
        );
        if ($validator->fails()) {
            return redirect('/options')
                ->withErrors($validator)
                ->withInput();
        }
        $option_name = $request->input('option_name');
        $option_category_id = $request->input('option_category_id');
        if ($option_category_id == "0") {
            $option_category = new OptionsCategories;
            $option_category->option_category_name = $option_name;
            $option_category->save();
        } else {
            $option = new Options;
            $option->option_name = $option_name;
            $option->options_categories_id = $option_category_id;
            $option->save();
        }

        return redirect()->back();
    }

    /**
     * Delete Option for Goods
     *
     * @param object $request Laravel Request object
     * 
     * @return \Illuminate\Http\Response
     */
    public function deleteOption(Request $request)
    {
        if (\Auth::user()->id !== 1) {
            return redirect()->back()->with(
                [
                    'message' => 'Nemate dozvolu za pristup!', 
                    'message_type' => 'danger'
                ]
            );
        }

        $option_id = $request->input('option_id');
        $options_goods = GoodsOptions::where('options_id', $option_id)->get();
        if ($options_goods->first()) {
            return redirect()->back()->with(
                [
                    'message' 
                    => 
                  'Ne možete da brišete opciju koja je prisutna u proizvodima!',
                  'message_type' => 'danger'
                ]
            );
        }
        Options::destroy($option_id);

        return redirect()->back();
    }

    /**
     * Delete Category of Options
     *
     * @param object $request Laravel Request object
     * 
     * @return \Illuminate\Http\Response
     */
    public function deleteOptionCategory(Request $request)
    {
        if (\Auth::user()->id !== 1) {
            return redirect()->back()->with(
                [
                    'message' => 'Nemate dozvolu za pristup!', 
                    'message_type' => 'danger'
                ]
            );
        }

        $option_category_id = $request->input('option_category_id');
        $options = Options::where(
            'options_categories_id', 
            $option_category_id
        )->get();
        if ($options->first()) {
            return redirect()->back()->with(
                [
                    'message' 
                  =>
                  'Ne možete da brišete kategoriju opcija koja postoje!',
                  'message_type' => 'danger'
                ]
            );
        }

        OptionsCategories::destroy($option_category_id);
        Options::where('options_categories_id', $option_category_id)->delete();

        return redirect()->back();
    }

    /**
     * Remove Option from Good
     *
     * @param object $request Laravel Request object
     * 
     * @return \Illuminate\Http\Response
     */
    public function removeOption(Request $request)
    {
        $option_id = $request->input('remove_option_id');
        $good_id = $request->input('remove_goods_id');
        GoodsOptions::where('options_id', $option_id)
            ->where('goods_id', $good_id)->delete();
        session(['focus' => $good_id]);

        return redirect()->back();
    }

    /**
     * Add Option to Good
     *
     * @param object $request Laravel Request object
     * 
     * @return \Illuminate\Http\Response
     */
    public function addOption(Request $request)
    {
        $option_id = $request->input('option_id');
        $good_id = $request->input('add_goods_id');
        GoodsOptions::create(['options_id' => $option_id, 'goods_id' => $good_id]);
        session(['focus' => $good_id]);

        return redirect()->back();
    }

    /**
     * Save/add image to instance of Goods, or delete image from instance of Goods
     *
     * @param object $request Laravel Request object
     * 
     * @return \Illuminate\Http\Response
     */
    public function goodImage(Request $request)
    {
        $good_id = $request->input('goods_id');
        if ($request->has('save')) {
            $imgData = json_decode($request->input("imgData"));
            $x_offset = intval($imgData->x);
            $y_offset = intval($imgData->y);
            $width = intval($imgData->width);
            $height = intval($imgData->height);
            $image = $request->file('image');
            $image_real_path = $image->getRealPath();
            $new_image_name 
                = '1'
                . sprintf("%04d", $good_id)
                . '.'
                . $image->getClientOriginalExtension();
            $good = Goods::find($good_id);
            $good->image = $new_image_name;
            $good->save();
            if ($width>$height) {
                $canvas = $width;
            } else {
                $canvas = $height;
            }
            Image::configure(array('driver' => 'imagick'));
            $file = Image::make($image_real_path);
            $file->crop($width, $height, $x_offset, $y_offset);
            $file->resizeCanvas($canvas, $canvas, 'center', false, 'ffffff');
            $file->resize(800, 800);
            $file->save(public_path('/images/goods/'.$new_image_name));
        } elseif ($request->has('delete')) {
            $good = Goods::find($good_id);
            if (file_exists(public_path('/images/goods/'.$good->image))) {
                unlink(public_path('/images/goods/'.$good->image));
            }
            $good->image = '';
            $good->save();
        }
        session(['focus'=>$good_id]);
        
        return redirect()->back();
    }

    /**
     * Disable Good if there a possible 
     * problem with it from showing in FrontEnd Store
     *
     * @param object $request Laravel Request object
     * 
     * @return \Illuminate\Http\Response
     */
    public function disableGood(Request $request)
    {
        $data = $request->all();
        array_shift($data);
        $good = Goods::find($data['goods_id']);
        if (array_key_exists('disable', $data)) {
            $good->disabled = 1;
        }
        if (array_key_exists('enable', $data)) {
            $good->disabled = 0;
        }
        $good->save();
        session(['focus'=>$data['goods_id']]);

        return redirect()->back();
    }

    /**
     * Show Manufacturers
     *
     * @return \Illuminate\Http\View
     */
    public function manufacturers()
    {
        $manufacturers = Manufacturer::orderBy('name', 'asc')->get();

        return view('manufacturers')->with(compact('manufacturers'));
    }

    /**
     * Create new Manufacturer
     *
     * @param object $request Laravel Request object
     * 
     * @return \Illuminate\Http\Redirect
     */
    public function saveManufacturer(Request $request)
    {
        $name = $request->input('name');
        Manufacturer::create(['name'=>$name]);

        return redirect()->back();
    }

    /**
     * Edit existing Manufacturer
     *
     * @param object $request Laravel Request object
     * 
     * @return \Illuminate\Http\Response
     */
    public function editManufacturer(Request $request)
    {
        $name = $request->input('name');
        $manufacturer_id = $request->input('manufacturer_id');
        if ($request->has('save')) {
            $manufacturer = Manufacturer::find($manufacturer_id);
            $manufacturer->name = $name;
            if ($request->hasFile('image')) {
                $imgData = json_decode($request->input("imgData"));
                $x_offset = intval($imgData->x);
                $y_offset = intval($imgData->y);
                $width = intval($imgData->width);
                $height = intval($imgData->height);
                $image = $request->file('image');
                $image_real_path = $image->getRealPath();
                $new_image_name 
                    = $manufacturer_id
                    . '.'
                    . $image->getClientOriginalExtension();
                Image::configure(array('driver' => 'imagick'));
                $file = Image::make($image_real_path);
                $file->crop($width, $height, $x_offset, $y_offset);
                if ($width>$height) {
                    $canvas = $width;
                } else {
                    $canvas = $height;
                }
                $file->resizeCanvas($canvas, $canvas, 'center', false, 'ffffff');
                $file->resize(200, 200);
                $file->save(public_path('/images/manufacturers/'.$new_image_name));
                $manufacturer->image = $new_image_name;
            }
            $manufacturer->save();
        } elseif ($request->has('delete')) {
            if (\Auth::user()->id !== 1) {
                return redirect()->back()->with(
                    [
                        'message' => 'Nemate dozvolu za pristup!',
                        'message_type' => 'danger'
                    ]
                );
            }

            $manufacturer = Manufacturer::find($manufacturer_id);
            $ppath = '/images/manfuacturers/' . $manufacturer->image;
            if (file_exists(public_path($ppath))) {
                unlink(public_path($ppath));
            }
            $manufacturer->image = '';
            $manufacturer->save();
            Manufacturer::destroy($manufacturer_id);
        }

        return redirect()->back();
    }

}
